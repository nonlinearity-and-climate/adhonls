%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Flat, arbitrary depth NLS extended to 4th order, in the space-like
% frame.(time evolution)
% See overleaf : https://www.overleaf.com/project/5f75a60dbb807800011d0594
% Based on Jansens dysthe deduction, extended to arbitrary depth.
%
% i B_X = -alpha_hat B_TT + bhat_D |B|^2B - i mu B + i (-alpha B_TTT + beta |A|^2
% A_T + beta_hat22 A^2 A^*_T) +Hhat3 ( hilbertlike term )
% all coefficients depend on kh in a cumbersome way, see References above 
%
% This code fixed, the frequency, the steepness and kh as known parameters,
% and calculates the rest. 
%
% The small parameter epsilon does NOT appear explicitely
%
% The reasonable normalization is to put g = 1, so that omega (FIXED!!!) is in unit of
% [m^-1/2] and set kh as a function of X (this IS the important parameter to discern focusing/defocusing regimes).
% Now omega^2 = k tanh kh = k sigma. We can derive h or k explicitely and
% find the dependence of cg, alpha_hat, bhat_D. 
% omega = 1 could as well be chosen. Any different omega = pippo corresponds to a
% rescaling of time (multiply by pippo^-1) and lengths (divide by pippo^2)
%
% The solution is made by means of integrating factor or interaction
% picture. 
%
% Based on the Code by Andrea Armaroli for Sedeltsky's model. 
% Alexis Gomel, Universite de geneve, 11/2021

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
%%
addpath(genpath(fullfile('functions','writers')));
addpath(genpath(fullfile('functions','Depth_parameters')));
addpath(genpath(fullfile('functions','import')));
addpath(genpath(fullfile('functions','plot')));
addpath(genpath(fullfile('functions','colormaps')));
addpath(genpath(fullfile('functions','khfunctions')));
addpath(genpath(fullfile('functions','Dialogs')));
addpath(genpath(fullfile('functions','sidebands')));
addpath(genpath(fullfile('functions','RK')));
addpath(genpath(fullfile('functions','jonswap')));
%%
g0 = 9.81;
crit= 1.363;
Normalized=1; %If =1 this makes the plots Normalized to B0. 
Debug=0;
%variables for plotting scale porpuses
	ratio=20/9;
	r1=400;
currentFolder = pwd; %set path to current folder.
Fsz=16; %Reference Fontsize 
[s_color,e_color]=color_language_beforeafter();
[color_s,color_Ns,color_utr,color_main,color_sideband,color_2sideband]=color_language_3wave();
[dispersion_color,nonlinear_color]=color_language_NLScoefs();

%Equiation coefficients ON/Off. 1 is ON, 2 is OFF
NLS_coef=onoff_dialog();
ON_OFF_coef=onoff_coef(NLS_coef);

DimensionOptions= {'Dimensional','Adimentional'}; 
defSelection = DimensionOptions{2};
Dimensionflag = bttnChoiseDialog(DimensionOptions, 'Input Style', defSelection,...
 'Input Style'); 
fprintf( 'User selection "%s"\n',DimensionOptions{Dimensionflag});

% selecting a function for kh 
switch Dimensionflag
     case 1 
        [params]=case1_param_dialog_nb()
		% normalized frequency omega = omega[s^-1]/g^{1/2}
        freq_dim = str2double(params(1));
		kh0=str2double(params(2));
        Omega = freq_dim*(2*pi)/sqrt(g0);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Tmax = str2double(params(3));
        Nper = str2double(params(4));
        nx = 2.^str2double(params(5));
        % number of poinxs to save
        Nt = str2double(params(6));
        esteep0 = str2double(params(7))
%         shoalingflag = str2double(params(8));
        Exportinit =  string(params(8));
        Factor=str2double(params(9));
%         dis=str2double(params(12));
        Out_time=str2double(params(10));
		%update dialog options
		print_button_dialog_parameters_case1_nb(freq_dim,kh0,Tmax,Nper,str2double(params(5)),Nt,esteep0,Exportinit,Factor,Out_time);

     case 2
        [params]=case2_param_dialog_nb()
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega = str2double(params(1));
		kh0=str2double(params(2));
        freq_dim = Omega*sqrt(g0)/(2*pi);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Tmax = str2double(params(3));
        Nper = str2double(params(4));
        nx = 2.^str2double(params(5));
        % number of points to save
        Nt = str2double(params(6));
        esteep0 = str2double(params(7));
%         shoalingflag = str2double(params(8));
        Exportinit =  string(params(8));
        Factor=str2double(params(9));
%         dis=str2double(params(12));
        Out_time=str2double(params(10));
		%update dialog options
		print_button_dialog_parameters_case1_nb(Omega,kh0,Tmax,Nper,str2double(params(5)),Nt,esteep0,Exportinit,Factor,Out_time);
end

mytlabel = '\tau';
myxlabel = '\xi';
myklabel = '\kappa';
myflabel = 'f';

khfunsel =  @khfun4;
f0=freq_dim;
FODflag = ON_OFF_coef(end); 

% bathymetry params
% hk = 1.363 is the focusing/defocusing turning point

%% solver params;
solvpar=solver_param_dialog();
ht0 = str2double(solvpar(1));
htmin = str2double(solvpar(2));
tol = str2double(solvpar(3));

%% compute the NLS parameters 
% DO NOT DISPLACE IT, it is necessary to estimate MI, soliton, DSW params
[H,K,Sigma,Cg,Cp,Alpha_hat,Bhat_D,Alpha_hat3,Alpha_hat4,Beta_hat21,Beta_hat22,Mu,Muintexp,HHat3] = depthdependent_HONLS_SpaceLike_calvert(Omega,kh0,ON_OFF_coef);
% plot_params_general(T,H,K,Mu,Cp,Cg,Muintexp,bhat_D,Alpha_hat,Alpha,Alpha4,Beta_hat21,Beta_hat22)
% ...and their initial values
[h0,k0,sigma0,cg0,cp0,alpha_hat,bhat_D,alpha_hat3,alpha40_hat,beta_hat21,beta_hat22,mu0,muintexp0,Hhat3] = depthdependent_HONLS_SpaceLike_calvert(Omega,kh0,ON_OFF_coef);
% Reference amplitude
% esteep=sqrt(2).*esteep0;
bhat=bhat_D-Hhat3/h0;
Bhat=Bhat_D-HHat3./H;
regime=bhat./alpha_hat;
central_frequency=k0;


U0 = esteep0./k0;%dummy var
wvl=2.*pi./k0;
a0=U0;
tst=0;
% Normalization scales
%nonlinear time and lenght are calculated with bhat instead of bhat_D to
%account for the contribution of the meanflow in the 3rd order when the
%depth is not deep enough. T0 = 1./(a0.^2.*bhat_D);
%L0=sqrt(abs(2*alpha_hat./bhat_D))./a0; now are 
T0 = abs(1./(a0.^2.*bhat));
L0 = sqrt(abs(2*alpha_hat./bhat))./a0;
Tnl=T0;
Lnl=L0;

%% definition of T lattice
Taumax=Tmax.*sqrt(g0); %transformation due to the adimentional frequency
if ht0>Taumax./Nt
    ht0 = Taumax./Nt;
end
Ht = Taumax./Nt;
Tau = 0:Ht:Taumax;
% Nper = 20;

kas0 = 1./L0;
% L0 = 2*pi/kas0;
% calculate the space window span
Lx = L0*Nper;
x = linspace(-Lx/2,Lx/2,nx+1);
x = x(1:end-1);

%%folders
[flm fullfolder]=save_filename_calvert(freq_dim,U0,esteep0,T0_dim);
mkdir(fullfolder) ;
filename_input=fullfile(fullfolder,strcat(flm,'_input.txt'));
% print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M);
% filename_wm=fullfile(fullfolder,strcat(flm,'_run_001.txt'));
% csvwrite(filename_wm,M) ;
filename_pos=fullfile(fullfolder,strcat(flm,'_pos.txt'));
% print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau);
mkdir(fullfile(fullfolder,'simulation and input'))
img_folder=fullfile(fullfolder,'simulation and input');
filename_params=fullfile(fullfolder,strcat(flm,'_params.txt'));
% print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
img_folder=fullfile(fullfolder,'simulation and input');
%% excitation type
inputOptions = {'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather',...
    'Peregrine Breather','Bright Soliton','Dark Soliton','Gaussian on top of a background','HPPW Maura'};%,...
defSelection = inputOptions{3};bttn_display=[6,3];
excflag = bttnChoiseDialog(inputOptions, 'Type of initial conditions', defSelection,...
 'Type of initial conditions'); 
fprintf( 'User selection "%s"\n', inputOptions{excflag});
switch excflag
    case 1
		IC_case=1;
        if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {sprintf('Noise fraction, B_0 = %g',B0)};
        dlg_title = 'Noisy Plane Wave';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1e-4'};        
        NPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=NPWpar;
        etanoise =  str2double( NPWpar(1));
        eta0 = 1-etanoise;
        U0 = str2double( NPWpar(1));
        variance = etanoise/nx*2/pi;
        omega0 = 1/Lnl*sqrt(2);
        windowwidth = 10*Lnl;
        
    case 2
		IC_case=2;
        if bhat_D./alpha_hat > 0; warning('Plane waves are stable'); end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\kappa / \kappa_Max'};
        dlg_title = 'Harmonically perturbed Plane Wave';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','1e-2','0e-3','pi/2','1'};
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=PPWpar;
        A0 = str2double( PPWpar(1));
        eta0 = A0 - str2double(PPWpar(2));
        unb = str2double(PPWpar(3));
        psi0 = eval(char(PPWpar(4)));
%         Kappa0 = str2double(PPWpar(5))*sqrt(A0).*(1-1.5*sqrt(A0)*esteep0);
		k3w_max=U0.*sqrt(abs(bhat./alpha_hat));
        Kappa0 = k3w_max.*str2double(PPWpar(5));
        eta1 = (A0-eta0+unb)/2;
        etam1 = (A0-eta0-unb)/2;     
        if eta1*etam1 < 0; error('The chosen unbalance is to big or negative intensity are chosen!');
		end 
        windowwidth = 10.*L0;
        
    case 3
		IC_case=3;
% 		IC_case=7;
        prompt = {'a','Initial point \tau_0'};
        dlg_title = 'Akhmediev Breather'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0.25','-500'};
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;
        aAB = str2double( ABpar(1));
        tau0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        KappaAB = 2*sqrt(1-2*aAB);
        Kappa0 =  KappaAB/L0;
        windowwidth = 10*L0;
%         prompt = {'\Kappa','Initial point \tau_0'};
%         dlg_title = 'Akhmediev Breather'; options.Interpreter = 'tex'; num_lines = 1;
%         defaultans = {'1.414','-10'};
%         IC_case=3;
%         ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);       
%         Kappa0 = str2double( ABpar(1));
%         T0AB = str2double( ABpar(2));
%         a = (1-(Kappa0/2)^2)/2; 
%         b = sqrt(8*a.*(1-2*a));      
% 		windowwidth = 10/Lnl;
    case 4
		IC_case=4;

% IC_case=8;
%         if nu0./lambda0 > 0
%             warning('Plane waves are stable');
%         end
        prompt = {'Initial point \tau_0'};
        dlg_title = 'Peregrine Breather';options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'-250'};
        Perpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);          
        save_params=Perpar;
        tau0 = str2double( Perpar(1));
        Kappa0 = 2*pi/Lnl;
        windowwidth = 30*Lnl;
        if Nper<20; warning('The time window is too narrow!'); end
 
	case 5
		IC_case=5;
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        xi0 = str2double(Solpar(1));
        Nsol = str2double( Solpar(2));
        Kappa0 = 2*pi/xi0/Lnl;
        windowwidth = 20*Lnl;
        if Nper<20; warning('The time window is too narrow!')
		end
		
	case 6 %Dark soliton
		IC_case=6;
% 		IC_case=10;
		if regime< 0 ,  warning('Dark soliton is not a solution'), end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};    
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        x0 = str2double(Solpar(1));        
        Nsol = str2double(Solpar(2));     
        Kappa0 = 2*pi/x0/Lnl;
        windowwidth = 20*Lnl;      
        if Nper<50 , warning('The time window is too narrow!'),end
	
	case 7
		IC_case=7;
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'),  end
        prompt = {'Perturbation width (in Tnl units)','Background rel. amplitude','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Gaussian on a background'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','0.5','1','1'};   
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        x0 = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));     
        Kappa0 = 2*pi/x0/Lnl;
        windowwidth = (19+x0)*Lnl; %this is for the plot.
% 		
% 	      if bhat_D./alpha_hat > 0; warning('Plane waves are stable'); end
	case 8

		IC_case=9;
		prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\kappa'};
        dlg_title = 'Perturbed Plane Wave (Maura-s bench)';
        options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'1','0.0078332','0.00013045','0','1'};
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=PPWpar;
        A0 = str2double( PPWpar(1));
        eta0 = A0 - str2double(PPWpar(2));
        unb = str2double(PPWpar(3));
        psi0 = eval(char(PPWpar(4)));
%         Kappa0 = str2double(PPWpar(5))*sqrt(A0).*(1-1.5*sqrt(A0)*esteep0);
		k3w_max=U0.*sqrt(abs(bhat./alpha_hat));
		
        Kappa0 = str2double(PPWpar(5));
	
        eta1 = (A0-eta0+unb);
        etam1 = (A0-eta0-unb);     
        if eta1*etam1 < 0; error('The chosen unbalance is to big or negative intensity are chosen!');
		end 
        windowwidth = 10.*L0;
		
    otherwise 
        error('No valid initial condition option');
end
k3w_max=U0.*sqrt(abs(bhat./alpha_hat));

% calculate the space window span
% Lx = 2*pi/Kappa0*Nper;
% Lx = 2*pi/k0*Nper;
% fprintf('Ltank = %g\n',Lx*L0);

% save_params_names=prompt;%TO BE USED TO SAVE PARAMETERS TO FILE. 
% fprintf('Tmax/T0 = %g\n',Tmax/T0);

%% definition of T lattice
% given the type of IC, we define omega0 and the time lattice
% omega0 is the characteristic  angular frequency dependent on the chosen initial
% conditions

hx = x(2)-x(1);
kappaxis = 2*pi.*linspace(-1/hx/2,1/hx/2,nx)';% kappaxis = kappaxis(1:end-1);   %%%%%%%%%%%%%%%%%%%%%%%%%%check here
%FFT phase shifts
% kx1 = linspace(0,nx/2 + 1,nx/2)';
% kx2 = linspace(-nx/2,-1,nx/2)';
% kx = ((2*pi/hx/nx)*[kx1; kx2]);
kx=fftshift(kappaxis);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% field initialization
u = zeros(nx,Nt+1);
uspectrum = u;
saveflag = zeros (Nt+1,1);
N = saveflag;
P = N;

%% Initial conditions
switch excflag
    case 1
        u(:,1) = U0*(sqrt(eta0) + sqrt(variance).*randn(size(x)) + 1i.*sqrt(variance).*randn(size(x)));
        
    case 2
		u(:,1) = U0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*Kappa0.*x).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*Kappa0.*x).*exp(1i.*psi0));

    case 3
%         u(:,1)= U0.*((1-4*a).*cosh(b*T0AB)+1i*b*sinh(b*T0AB)+sqrt(2*a).*cos(Kappa0.*x))./(sqrt(2*a).*cos(Kappa0.*x)-cosh(b.*T0AB));
          u(:,1)  = U0.*(1 + (KappaAB^2/2.*cosh(bAB*tau0/Tnl) - 1i*bAB*sinh(bAB.*tau0/Tnl))./(sqrt(2*aAB)*cos(KappaAB*x/Lnl) - cosh(bAB.*tau0/Tnl))).*exp(1i*tau0/Tnl);    

    case 4
		 % Peregrine Soliton
        % TP = sqrt(abs(2*lambda0./nu0))./B0;
        % Lnl = 1./nu0/B0^2;
        % X0 = -500;
        u(:,1) = U0.*(1-4.*(1-2i.*tau0./Tnl)./(1+4*(x./Lnl).^2 + 4*(tau0/Tnl)^2)).*exp(-1i*tau0/Tnl);
	case 5	
% 		u(:,1) = Nsol*(U0/tau0).*sech((U0/tau0)*sqrt(abs(bhat_D/2/alpha_hat)).*x);
%       u(:,1) = U0.*Nsol/tau0.*sech(x/tau0);		
        % Bright Soliton
        % soliton order     % Nsol = 3;
        u(:,1) = Nsol*(U0/xi0).*sech((U0/xi0)*sqrt(abs(bhat_D/2/alpha_hat)).*x);
		
	case 6	%dark soliton  
		u(:,1) = Nsol*a0*tanh(a0*sqrt(abs(bhat/2/alpha_hat)).*x);

	case 7 
% 		u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB)*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0/Lnl);    
        u(:,1)  = U0*(Bback + Bpert*exp(-(x./Lnl/x0).^(2*MSG)));
   
	
	case 8 %%%%%Debibe's case start. I have to define U0, steepness and all the rest from the simulation variables. 
	

		filedebs=fullfile('Maura_benchmark','augost','sims_toghether');
		if kh0==2.5
			debsdata='shallow_aug.mat';
			Kappa0=0.35717;
		elseif kh0==5
			debsdata='interm_aug.mat';
			Kappa0=0.50004;
		else
			debsdata='deep_aug.mat';
			Kappa0=0.57147;
		end
		debs_vars=load(fullfile(filedebs,debsdata));
		% etadeb=fliplr(debs_vars.eta-mean(debs_vars.eta')');
		etadeb=(debs_vars.eta-mean(debs_vars.eta')')';

		t_deb=debs_vars.t;
		x_deb=debs_vars.x-max(debs_vars.x)./2;
		udeb=hilbert(etadeb);
		% xdebclean=abs(x_deb)<=15;
		xdebclean=abs(x_deb)>=0;
		
		
		tst_0=1.2*T0/sqrt(g0);%workd on 250

		tst=tst_0;
		[val,idx]=min(abs(t_deb-tst));		
		
		%%
		data_series=etadeb(1:end,:);
		var=x_deb(1:end);
		sz=size(data_series);
		dsl=sz(1);
		data_gauge=idx;%workd on 1.
		kappaxis_deb=2*pi.*linspace(-1/(x_deb(2)-x_deb(1))/2,1/(x_deb(2)-x_deb(1))/2,dsl)';
		pp=0;
		[ModesAmplitudes_d,fmod,ind1,ind_dist]=getsidebands_static_maura_surf(etadeb,var,data_gauge,pp,kh0);
% 		[ModesAmplitudes_d,fmod,ind1,ind_dist]=getsidebands_static_maura_surf(udeb,var,data_gauge,pp);ModesAmplitudes_d=ModesAmplitudes_d./2;
		
		%%
		car0=abs(ModesAmplitudes_d(1,:));
		s10=abs(ModesAmplitudes_d(2,:));
		sm10=abs(ModesAmplitudes_d(3,:));
		s20=abs(ModesAmplitudes_d(4,:));
		sm20=abs(ModesAmplitudes_d(5,:));
		s30=abs(ModesAmplitudes_d(6,:));
		sm30=abs(ModesAmplitudes_d(7,:));
		s40=abs(ModesAmplitudes_d(8,:));
		sm40=abs(ModesAmplitudes_d(9,:));
		
		Mwin=4;
		car = movmean(car0,[Mwin Mwin]);
		s1 = movmean(s10,[Mwin Mwin]);
		sm1 = movmean(sm10,[Mwin Mwin]);
		s2 = movmean(s20,[Mwin Mwin]);
		sm2 = movmean(sm20,[Mwin Mwin]);
		s3 = movmean(s30,[Mwin Mwin]);
		sm3 = movmean(sm30,[Mwin Mwin]);
		s4 = movmean(s40,[Mwin Mwin]);
		sm4 = movmean(sm40,[Mwin Mwin]);

		Psid_main =unwrap(angle(ModesAmplitudes_d(1,:)));
		Psid_left =unwrap(angle(ModesAmplitudes_d(3,:)));
		Psid_right=unwrap(angle(ModesAmplitudes_d(2,:)));
		Psid_2 =unwrap(angle(ModesAmplitudes_d(4,:)));
		Psid_m2 =unwrap(angle(ModesAmplitudes_d(5,:)));
		Psid_3 =unwrap(angle(ModesAmplitudes_d(6,:)));
		Psid_m3 =unwrap(angle(ModesAmplitudes_d(7,:)));
		Psid_4 =unwrap(angle(ModesAmplitudes_d(8,:)));
		Psid_m4 =unwrap(angle(ModesAmplitudes_d(9,:)));
		
		Psid_exp =unwrap(unwrap( Psid_left+Psid_right) - 2.*Psid_main);
		Psid_exp2 =unwrap(unwrap( Psid_2+Psid_m2 ) - 2.*Psid_main);

% 		Norm_modes_d0=trapz(var',abs(hilbert(data_series(:,idx))).^2)./(var(end)-var(1));
%%
		
		%%%reconstruc from HOSM
		etad_rec=car.*exp(1i.*Psid_main) + s1.*exp(1i*Kappa0.*x_deb').*exp(1i.*Psid_right) +  sm1.*exp(-1i*Kappa0.*x_deb').*exp(1i.*Psid_left)+s2.*exp(1i*2*Kappa0.*x_deb').*exp(1i.*Psid_2)+sm2.*exp(-1i*2*Kappa0.*x_deb').*exp(1i.*Psid_m2)+...
			s3.*exp(1i*3*Kappa0.*x_deb').*exp(1i.*Psid_3)+sm3.*exp(-1i*3*Kappa0.*x_deb').*exp(1i.*Psid_m3)+s4.*exp(1i*4*Kappa0.*x_deb').*exp(1i.*Psid_4)+sm4.*exp(-1i*4*Kappa0.*x_deb').*exp(1i.*Psid_m4);
		etad_rec=real(etad_rec.*exp(1i*k0.*x_deb'));
		
% 		Norm_modes_d0=(car.^2+s1.^2+sm1.^2+s2.^2+sm2.^2);
		Norm_modes_d0=trapz(x_deb',abs(hilbert(etad_rec(:,:))).^2)./(x_deb(end)-x_deb(1));

		a0_norm=sqrt(Norm_modes_d0(idx));
% 		Kappa0=fmod;
		esteep0=k0.*a0_norm;%dummy var
		U0=a0_norm; %defined by the norm at the point. 
		a0=U0;
		
		
		ud_rec=hilbert(etad_rec);
		ud_rec2=hilbert(etad_rec+0.5*k0*real(etad_rec.^2.*exp(1i.*2.*k0.*x_deb')));
		
% 		udeb_rec=(car.*exp(1i.*Psid_main(idx)) + s1.*exp(1i*Kappa0.*x).*exp(1i.*Psid_right(idx)) +  sm1.*exp(-1i*Kappa0.*x).*exp(1i.*Psid_left(idx))+s2.*exp(1i*2*Kappa0.*x).*exp(1i.*Psid_2(idx))+sm2.*exp(-1i*2*Kappa0.*x).*exp(1i.*Psid_m2(idx))+...
			%s3.*exp(1i*3*Kappa0.*x).*exp(1i.*Psid_3(idx))+sm3.*exp(-1i*3*Kappa0.*x).*exp(1i.*Psid_m3(idx)));
% 		eta_rec=car(idx).*exp(1i.*Psid_main(idx)) + s1(idx).*exp(1i*Kappa0.*x).*exp(1i.*Psid_right(idx)) +  sm1(idx).*exp(-1i*Kappa0.*x).*exp(1i.*Psid_left(idx))+s2(idx).*exp(1i*2*Kappa0.*x).*exp(1i.*Psid_2(idx))+sm2(idx).*exp(-1i*2*Kappa0.*x).*exp(1i.*Psid_m2(idx))+...
% 			s3(idx).*exp(1i.*3*Kappa0.*x).*exp(1i.*Psid_3(idx))+sm3(idx).*exp(-1i.*3*Kappa0.*x).*exp(1i.*Psid_m3(idx))+s4(idx).*exp(1i.*4*Kappa0.*x).*exp(1i.*Psid_4(idx))+sm4(idx).*exp(-1i.*4*Kappa0.*x).*exp(1i.*Psid_m4(idx));
% 		eta_rec=real(etad_rec.*exp(1i*k0.*x'));
% 		u_rec=hilbert(eta_rec);
% 		u(:,1) =u_rec(:,1);
 		u2 = (car(idx).*exp(1i.*Psid_main(idx)) + s1(idx).*exp(1i*Kappa0.*x).*exp(1i.*Psid_right(idx)) +  sm1(idx).*exp(-1i*Kappa0.*x).*exp(1i.*Psid_left(idx))+s2(idx).*exp(1i*2*Kappa0.*x).*exp(1i.*Psid_2(idx))+sm2(idx).*exp(-1i*2*Kappa0.*x).*exp(1i.*Psid_m2(idx))+...
			s3(idx).*exp(1i*3*Kappa0.*x).*exp(1i.*Psid_3(idx))+sm3(idx).*exp(-1i*3*Kappa0.*x).*exp(1i.*Psid_m3(idx))+s4(idx).*exp(1i*4*Kappa0.*x).*exp(1i.*Psid_4(idx))+sm4(idx).*exp(-1i*4*Kappa0.*x).*exp(1i.*Psid_m4(idx)));
		u(:,1) =u2;
		Psi0=-Psid_main(idx);
		rec_surf1=u(:,1).*exp(1i.*k0.*(x'+Psi0));
		rec_surf2=0.5.*k0.*(u(:,1).*exp(1i.*k0.*(x'+Psi0))).^2;
		RS=rec_surf1+rec_surf2;
%%
		f=figure()
		subplot(121)
		sp=pcolor(x_deb,t_deb,abs(ud_rec'));
		colormap 'viridis'
		set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
		title('Envelope reconstruction from HOSM')
		colorbar
		ylim([0,2000])
		xlim([-20,20])
		subplot(122)
		hold on
		plot(x_deb,abs(ud_rec(:,idx)),'b')
		plot(x,abs(u2),'k')
		xlim([-20,20])
		hold off
% 		%%
% 		if Debug==1
% 			f=figure()
% 			subplot(121)
% 			hold on
% 			plot(x_deb,etadeb(:,idx),'b','DisplayName','Raw HOSM')
% 			plot(x_deb,etad_rec(:,idx),'r','DisplayName','Reconstructed from 7-wave')
% 			plot(x,real(u(:,1).*exp(1i.*k0.*x')),'k','DisplayName','Reconstructed from init envelope')
% 			hold off
% 			legend()
% 			ylim([0,0.02])
% 			xlim([-20,20])
% 			subplot(122)		
% 			hold on
% 			plot(x,abs(RS),'b')
% 			plot(x_deb,abs(udeb(:,idx)),'r')
% 			hold off
% 			xlim([-20,20])
% 		end
		%%

		Eta0d = abs(ModesAmplitudes_d(1,:)).^2./U0^2;
		Eta1d = abs(ModesAmplitudes_d(2,:)).^2./U0^2;
		Etam1d = abs(ModesAmplitudes_d(3,:)).^2./U0^2;
		Eta2d = abs(ModesAmplitudes_d(4,:)).^2./U0^2;
		Etam2d = abs(ModesAmplitudes_d(5,:)).^2./U0^2;
		Gammahat_deb = Eta1d - Etam1d;
		Utrd = Eta0d + Eta1d + Etam1d;
		plot_3w_Mura(t_deb,Eta0d,Eta1d,Etam1d,Eta2d,Etam2d, Utrd,Psid_exp,tst,Tmax,Fsz)
		%%
		figure('position',[20 0 ratio*r1 1.3*r1])
		set_latex_plots(groot)
		subplot(211)	
		plot(t_deb,car,'LineWidth',1.6)
		xlim([0,100])
		ylabel('Carrier Amplitude [m]','FontSize',Fsz)
		xlabel('$\tau$ [s]','FontSize',Fsz)
		set(gca,'FontSize',Fsz-3)

% 		plot(var,abs(data_series(:,idx)),'b')
% 		title('Carrier','FontSize',Fsz-1)		
		subplot(212)
		hold on
		plot(t_deb,max(data_series))
		plot([t_deb(idx),t_deb(idx)],[0.01,0.05],':k','LineWidth',1.6)
		hold off
		xlabel('$\tau$ [s]','FontSize',Fsz)
		ylabel('Max wave height [m]','FontSize',Fsz)
		yt = yticks;
		xt = xticks;
		
		set(gca, 'xtick', sort([xt, t_deb(idx)]), 'Ycolor', 'k','YDir','normal','FontSize',Fsz-3)
		xtickformat('%.f');
% 		subplot(133)
% 		hold on 
% 		plot(t_deb,Eta0d)
% 		plot(t_deb,Eta1d)
% 		plot(t_deb,Etam1d)
% 		xlabel('$\tau$ [s]','FontSize',Fsz)
		hold off
%%

		% Normalization scales
		%nonlinear time and lenght are calculated with bhat instead of bhat_D to
		%account for the contribution of the meanflow in the 3rd order when the
		%depth is not deep enough. T0 = 1./(a0.^2.*bhat_D);
		%L0=sqrt(abs(2*alpha_hat./bhat_D))./a0; now are 
		T0 = abs(1./(a0.^2.*bhat));
		L0 = sqrt(abs(2*alpha_hat./bhat))./a0;
		Tnl=T0;
		Lnl=L0;

		kas0 = 1./L0;
% 		Lx = L0*Nper;
% 		x = linspace(-Lx/2,Lx/2,nx+1);
% 		x = x(1:end-1);

		%%folders
	% 	[flm fullfolder]=save_filename(freq_dim,U0,esteep0,T0_dim);
		%%% definition of T lattice
		hx = x(2)-x(1);
		kappaxis = 2*pi.*linspace(-1/hx/2,1/hx/2,nx)';% kappaxis = kappaxis(1:end-1);   %%%%%%%%%%%%%%%%%%%%%%%%%%check here
		%FFT phase shifts
		kx=fftshift(kappaxis);

		%%% field initialization
		u = zeros(nx,Nt+1);
		uspectrum = u;
		saveflag = zeros (Nt+1,1);
		N = saveflag;
		P = N;

		k3w_max=U0.*sqrt(abs(bhat./alpha_hat));
		u(:,1) =u2;
		fa=length(x_deb)./(x_deb(end)-x_deb(1));
% 		u2 = (car(idx).*exp(1i.*Psid_main(idx)) + s1(idx).*exp(1i*Kappa0.*x).*exp(1i.*Psid_right(idx)) +  sm1(idx).*exp(-1i*Kappa0.*x).*exp(1i.*Psid_left(idx))+s2(idx).*exp(1i*2*Kappa0.*x).*exp(1i.*Psid_2(idx))+sm2(idx).*exp(-1i*2*Kappa0.*x).*exp(1i.*Psid_m2(idx))+...
% 			s3(idx).*exp(1i*3*Kappa0.*x).*exp(1i.*Psid_3(idx))+sm3(idx).*exp(-1i*3*Kappa0.*x).*exp(1i.*Psid_m3(idx))+s4(idx).*exp(1i*4*Kappa0.*x).*exp(1i.*Psid_4(idx))+sm4(idx).*exp(-1i*4*Kappa0.*x).*exp(1i.*Psid_m4(idx)));
% 		u(:,1) =u2;
%%
		figure()
		subplot(211)
		Psi0=-Psid_main(idx);
		rec_surf1=u(:,1).*exp(1i.*k0.*(x'+Psi0));
		rec_surf2=0.5.*k0.*(u(:,1).*exp(1i.*k0.*(x'+Psi0))).^2;
		RS=rec_surf1+rec_surf2;
		hold on
		plot(x,abs(u(:,1).*exp(1i.*Psi0)),'DisplayName','IC from spectrum')%+esteep0.^2.*abs(u(:,1).*exp(1i.*Psi_exp(idx)))
	% 	plot(x,abs(utest.*exp(1i.*Psi0)),'DisplayName','IC 1st from spectrum')%+esteep0.^2.*abs(u(:,1).*exp(1i.*Psi_exp(idx)))
		% 	plot(x,abs(u(:,1).*exp(1i.*Psi_exp(idx))+(esteep0.*u(:,1).*exp(1i.*Psi_exp(idx))).^2),'DisplayName','IC 2nd order')%+esteep0.^2.*abs(u(:,1).*exp(1i.*Psi_exp(idx)))
	% 	plot(x,real(RS),'DisplayName','Surf elev from spectrum')%+esteep0.^2.*abs(u(:,1).*exp(1i.*Psi_exp(idx)))
		plot(x,abs(RS),'DisplayName','2nd IC from spectrum')%+esteep0.^2.*abs(u(:,1).*exp(1i.*Psi_exp(idx)))
		plot(var+max(var),data_series(:,idx)',':k','DisplayName','raw data Maura')
		plot(var+max(var),abs(hilbert(data_series(:,idx)')),'k','DisplayName','HilbertData')
	% 	plot(var,abs(udeb_filt),'DisplayName','HilbertData')
		legend()
		hold off
		xlim([0,max(var+max(var))])
		xlabel('$x$[m]')
		title('Init dabbie sanity check')

		UdBth=-25;
		pp=1;
		hvar=var(2)-var(1);
		padding_points=(dsl+1)*2^pp-1; %Amount of padding.
		foudata=2*fftshift(fft(data_series(:,idx),[],1),1)./dsl;
		new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,dsl)';
		UkspHOSM_0 = 10*log10(abs(foudata./U0));
		UkspHOSM_0 = (UkspHOSM_0>UdBth).*(UkspHOSM_0-UdBth) +  UdBth;
		ucurr_spec=ifftshift(ifft(u(:,1),[],1),1);
		Uksp2_0 = 10*log10(abs(ucurr_spec./U0));
		Uksp2_0 = (Uksp2_0>UdBth).*(Uksp2_0-UdBth) +  UdBth;
		xplot = abs(x)<=windowwidth;
		xclean = abs(x)<=2.5*windowwidth; %array cleaning the borders to have better analysis. this is approximate
		kplot= abs(kappaxis)<=2.*pi/L0;
	%         semilogy(kappaxis,abs(ucurr_spec./U0));
% 		xlim([-2.*pi/L0,2.*pi/L0]);
  		
		subplot(212)
		hold on
		plot(new_omegaxis,UkspHOSM_0 );
		plot(kappaxis(kplot)+5, Uksp2_0(kplot),'Linewidth',1.5);
% 		plot(5,car,'x')
% 		plot(5+Kappa0,s1,'x')
% 		plot(5-Kappa0,sm1,'x')
% 		plot(5+2*Kappa0,s2,'x')
% 		plot(5-2*Kappa0,sm2,'x')
		hold off
		xlabel('$k \, [m^-1]$','Interpreter','latex','fontsize',Fsz);
		xlim([1.8,10.2])
		ylabel('$dB$','Interpreter','latex','fontsize',Fsz); 

		%%%%% case 9 end
	
	
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization of computation
ucurr = u(:,1);
uspectrum(:,1) = ifftshift(ifft(ucurr,[],1),1);

pind = 1;
N(pind) = trapz(x,abs(ucurr).^2);
P(pind) = trapz(x,0.5i.*(fft(-1i.*kx.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kx.*ifft(conj(ucurr))).*ucurr));
t = 0;
ht = ht0;
pind = 2;
count = 0;

xplot = abs(x)<=windowwidth;
xclean = abs(x)<=2.5*windowwidth; %array cleaning the borders to have better analysis. this is approximate
kplot= abs(kappaxis)<=2.*pi/L0;

%% 
% figure()
% subplot(131)
% v1=abs(ifft(ifftshift(-1i.*kx.*fftshift(fft(conj(ucurr))))));
% plot(x,v1);
% 
% 
% subplot(132)
% kt1 = linspace(0,nx/2 + 1,nx/2)';
% kt2 = linspace(-nx/2,-1,nx/2)';
% kx_with0 = ((2*pi/hx/nx)*[kt1; kt2]);
% hold on
% % plot(x,abs(fft(-1i.*kx_with0.*ifft(conj(ucurr)))));
% v2=abs(ifft(ifftshift(-1i.*kx_with0.*fftshift(fft(conj(ucurr))))));
% plot(x,v2);
% hold off
% subplot(133)
% plot(x,abs(v1-v2))
%%
f=figure('name','First last','position',[20 20 500 500]);
plot_periods=5;
subplot(2,2,1)
hold on
plot(x(xplot),abs(u(xplot,1)),'DisplayName','U')
% plot(x,real(u(:,1).*cos(k0.*x')))
% plot(x,a0.*real(u(:,1).*exp(1i.*k0.*x')));
% xlim([-plot_periods*L0,plot_periods*L0])
% xlim([-windowwidth ,windowwidth ])
ylim([0,3*a0])
xlabel('$\xi$')
legend()

hold off
title('initial condition space')
subplot(2,2,2)
spectrum=fftshift(fft(u(:,1)))./nx;
max_spec=max(abs(spectrum(:,1)));


%%%%%%%%%%%%%%%%%%%%%%% fix this plot and add it to the other code too.
if any(IC_case==[1,2,3,5,8,9])
	kappa_max=k3w_max;
% 	kappa_ax= abs(kappaxis)<=sqrt(2).*(kappa_max+k0);
	kappa_ax=linspace(-sqrt(2).*kappa_max,sqrt(2).*kappa_max,200);
	Gain=abs(bhat_D.*kappa_ax.*kappa_max.*sqrt(2-(kappa_ax./kappa_max).^2));
	Gain=Gain./max(Gain);
	Gain_beta=abs(bhat.*kappa_ax.*kappa_max.*sqrt(2-(kappa_ax./kappa_max).^2));
	Gain_beta=Gain_beta./max(Gain_beta);
	hold on
	plot(kappa_ax+k0,Gain,'--r')
	plot(kappa_ax+k0,Gain_beta,'--b','LineWidth',1.5)
	plot([kappa_max+k0 kappa_max+k0],[0 1],':k')
	plot([Kappa0+k0 Kappa0+k0],[0 1],':r','LineWidth',1.5)
	plot([-Kappa0+k0 -Kappa0+k0],[0 1],':r','LineWidth',1.5)
	plot([-kappa_max+k0 -kappa_max+k0],[0 1],':k')
	sp=plot(kappaxis+k0,abs(abs(spectrum(:,1))/max_spec) );
	hold off
	xlim([-2*kappa_max+k0,2*kappa_max+k0])
else 
	UdBth=-30;
	max_spec=max(abs(spectrum(:,1)));
	[ d, ix ] = min( abs( abs(spectrum(:,1)) - max_spec ) );%find the critical value index
	spectrum(ix,:)=0.5*spectrum(ix-1,:)+0.5*spectrum(ix+1,:);
	hold on
	%     UdBth = -15;
	Usp = 10*log10(abs(spectrum.'));
	Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	sp=plot(kappaxis+k0,Usp );
	xlim([-3*kas0+k0,3*kas0+k0])
end
ylabel('db')
xlabel('k')
% plot(kappaxis,abs(uspectrum(:,1)));
title('initial condition fourier')
% xlim([-2*pi/L0+k0,2*pi/L0+k0])
% xlabel('k')

subplot(2,2,3)
hold on
plot(kappaxis,abs(fft(abs(kappaxis).*ifft(abs(u(:,1)).^2))),'DisplayName','$$H(\partial_x |U|^2)$$')
% plot(kappaxis,abs(fft(((kx+k0)./tanh(h0.*(kx+k0))).*ifft(abs(ucurr).^2))),'DisplayName','F(\coth{kh+k0}F^{-1}a_x |U|^2)')
plot(kappaxis,abs(fft((kappaxis./tanh(h0.*(kappaxis))).*ifft(abs(u(:,1)).^2))),'DisplayName','$$F(\coth{(kh)}F^{-1}\partial_x |U|^2)$$')
% plot(kappaxis,ifft(ifftshift((kappaxis./h0)./(tanh(h0.*(kappaxis))).*fftshift(fft(abs(u(:,1)).^2))) ),'DisplayName','from RK')
plot(kappaxis,abs(fft(fftshift(abs(kappaxis).*ifftshift(ifft(abs(u(:,1)).^2)))))) 
hold off
xlim([-windowwidth ,windowwidth ])
ylim([0,3*a0])
title('Mean flow comparisson')
legend('interpreter','latex')

subplot(2,2,4)
hold on
plot(kappaxis,abs(kappaxis./tanh(h0.*kappaxis)),'DisplayName','|k*coth {kh}|')
plot(kappaxis,abs(kappaxis),'DisplayName','|k|')
hold off
xlim([-Nper/2.*2*pi/k0,Nper/2.*2*pi/k0])
xlabel('k')
title('Convolution term')
legend()
% 
% alpha4_hat0=0	;

%% plot parameters to compare with deep water.
fig_param=plot_parameterspace(kappaxis,Omega,kh0,ON_OFF_coef);
savefig(fullfile(img_folder,strcat(flm,'_parameters')));

%% Computation
Fsz=16;
UdBth = -35;
ucurr_spec=ifftshift(ifft(ucurr,[],1),1);%./nx;
uspectrum(:,pind) = ucurr_spec;
Uksp2_0 = 10*log10(abs(ucurr_spec./U0));
Uksp2_0 = (Uksp2_0>UdBth).*(Uksp2_0-UdBth) +  UdBth;
	 
shoalflag=0;
f=figure('name','First last','position',[20 15 900 600]);
set_latex_plots(groot)
filename_gif = 'NLSAB.gif';
set(0,'defaulttextfontsize',Fsz);

kappa_ax_gain= abs(kappaxis)<=sqrt(2).*(abs(k3w_max));
Gain=abs(bhat_D.*kappa_ax_gain.*k3w_max.*sqrt(2-(kappa_ax_gain./k3w_max).^2));
Gain=Gain./max(Gain);
while t<Taumax
        err = tol*10;
        numberofrejstep = 0;
        while err>tol
			[ucurrnew,err] = DV4ORK34_spacelike_shiftAD_calvert(ht,ucurr,kappaxis,h0,alpha_hat,bhat_D,alpha_hat3,Alpha_hat4,beta_hat21,beta_hat22,Hhat3,kh0);
            if (err<tol)
                t = t + ht;
                ucurr = ucurrnew;
                count = count + 1;
                step(count) = ht;
            else
                numberofrejstep =  numberofrejstep + 1;
                if numberofrejstep > 10
                    fprintf('10!\n');
                end
			end
			ht = min(max(0.5,min(2,.95*(tol./err).^.25)).*ht,Ht);
            if ht < htmin 
                error('Integration step too small');
            end
            if isnan(err)
                error('Diverges!');
            end
        end
    if isnan(ucurr)
        error('diverges!!!');
	end
     %%%print result
     if t>=Tau(pind) && saveflag(pind) == 0
        saveflag(pind) = 1;   
        N(pind) = trapz(x,abs(ucurr).^2);
        P(pind) = trapz(x,0.5i.*(fft(-1i.*kx.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kx.*ifft(conj(ucurr))).*ucurr) + 0*esteep0* abs(ucurr).^4);
		meanflow=Hhat3.*abs(ucurr.*fft(fftshift(((kappaxis)./tanh(h0.*(kappaxis))).*ifftshift(ifft(abs(ucurr).^2)))));
		
		omx2=kappaxis.*tanh(h0.*(kappaxis));
		sigx=tanh(h0.*(kappaxis));
		cgx2= ((sigx + kappaxis.*h0.*(1-sigx.^2))/2).^2./omx2;
		calvert=Hhat3.*1i.*ifft(ifftshift((kappaxis)./(tanh(h0.*(kappaxis)).*(1-cgx2.*kappaxis./(tanh(h0.*(kappaxis))))).*fftshift(fft(abs(ucurr).^2))));
        if max(abs(ucurr))*k0>0.4
            warning('Wave breaking probably occurs!');
        end

		u(:,pind) = ucurr;
		% computing spectrum
		ucurr_spec=ifftshift(ifft(ucurr,[],1),1);
		uspectrum(:,pind) = ucurr_spec;
		Tau(pind) = t;
% 		timeevolution(x, t,kappaxis,u(:,pind),u(:,1),L0,T0,a0,k0,Dimensionflag);
		Uksp2 = 10*log10(abs(ucurr_spec./U0));
		Uksp2 = (Uksp2>UdBth).*(Uksp2-UdBth) +  UdBth;
		
		subplot(131)
		plot(x(xplot),abs(u(xplot,pind)),x(xplot),abs(u((xplot),1)),'Linewidth',1.5);
% 		xlim([-windowwidth,windowwidth]);
		%         xlabel('\xi','fontsize',14); 
		ylabel('$U$[m]','Interpreter','latex','fontsize',Fsz); 
		set(gca,'fontsize',Fsz-1);
		ylim([0,3*a0]);
% 			ylabel('$\epsilon$','Interpreter','latex','fontsize',14); 
%         set(gca,'fontsize',13);
% 		ylim([0,3*esteep0]);
		title(strcat('kh=',num2str(kh0,'%.2f'),', $$\epsilon =$$',num2str(max(abs(ucurr(xplot))).*k0,'%.2f')),'interpreter','latex','fontsize',Fsz-5);
% 		legend('interpreter','latex')
		xlabel('$\xi \, [m]$ ','Interpreter','latex','fontsize',Fsz);

		subplot(132)
%         semilogy(kappaxis,abs(ucurr_spec./U0));
		plot(kappaxis(kplot),Uksp2(kplot),kappaxis(kplot), Uksp2_0(kplot),'Linewidth',1.5);
		xlabel('$k \, [m^-1]$','Interpreter','latex','fontsize',Fsz);
% 		xlim([-2.*pi/L0,2.*pi/L0]);
  		ylim([-30,0]);
		title(strcat(num2str(100.*t./Taumax,'%.1f'),' \% Sim  ',', t = ',num2str(t,'%.1f'),'s'),'interpreter','latex','fontsize',Fsz-5);
		ylabel('$dB$','Interpreter','latex','fontsize',Fsz); 

		
		subplot(433)
		% plot(kx,abs(Hhat3p.*1i.*ucurr.*fft(1./(tanh(h0.*abs(kx+k0))).*ifft(abs(ucurr).^2))))
		plot(x,(Omega.*k0./2).*abs(ucurr.*fft(fftshift(abs(kappaxis).*ifftshift(ifft(abs(ucurr).^2))))),'b','DisplayName','$UH(a_x |U|^2)$')
% 		xlim([-plot_periods.*L0,plot_periods.*L0])
        xlim([-windowwidth,windowwidth]);
		title('$$\frac{\omega k_0}{2} U H[\partial_x |U|^2]$$','interpreter','latex','fontsize',Fsz-5);

		subplot(436)	
		plot(x(xplot),meanflow(xplot),'b','DisplayName','$$UF(\coth{kh}F^{-1}\partial_x |U|^2)$$')
% 		xlim([-plot_periods.*L0,plot_periods.*L0])
        xlim([-windowwidth,windowwidth]);
% 		legend()
		title('$ \hat{H}_3 U F[\coth{kh} \quad F^{-1}[\partial_x |U|^2]$','interpreter','latex','fontsize',Fsz-5);

		subplot(439)		
		plot(x(xplot),bhat_D.*abs(ucurr(xplot).*abs(ucurr(xplot)).^2),'b','DisplayName','$\beta_D U|U|^2$')
% 		xlim([-plot_periods.*L0,plot_periods.*L0])
        xlim([-windowwidth,windowwidth]);
% 		xlabel('\xi')
		title('$U|U|^2$','fontsize',Fsz-5);

		subplot(4,3,12)		
		plot(x(xplot),calvert,'b','DisplayName','calvert ')
        xlim([-windowwidth,windowwidth]);
		xlabel('$\xi$')
		title(strcat('Calvert',num2str(bhat_D.*H/Hhat3)),'interpreter','latex','fontsize',Fsz-5);
			
		drawnow
		sgtitle(dlg_title)
		
		F = getframe(gcf);
        im = frame2im(F);
        [imind,cm] = rgb2ind(im,256);
		if pind == 2;
           imwrite(imind,cm,filename_gif,'gif', 'DelayTime',.12,'Loopcount',inf); 
        else         
           imwrite(imind,cm,filename_gif,'gif','WriteMode','append','DelayTime',.12);
		end
		pind = pind + 1;

	 end    
end

%% recover the old variables (see B above)
if any(IC_case==[8,9])
% 	len_xclean=2^8;
	temp_x=x-x(1);
	xclean = abs(x)<=temp_x(2^10); %array cleaning the borders to have better analysis. this is approximate
end
T=Tau./sqrt(g0);% computing k spectrum
uksp = uspectrum;
% omega spectrum
t_uspectrum = fftshift(fft(u(xclean,:),[],2),2)./length(T);%/Nt;
freqax = linspace(-1/Ht/2,1/Ht/2,Nt+1) + f0;
% 
% switch Dimensionflag
%     case 'Adimensional'
%         mytlabel = '$\tau$';
%         myxlabel = '$\xi$';
%         myklabel = '$\kappa$';
%         myflabel = '$f$';
%     case 'Dimensional'
%         mytlabel = '$t$ [s]';
%         myxlabel = '$x$ [m]';
%         myklabel = '$\kappa$ [m^-1]';
%         myflabel = '$f$ [Hz]';
% end
%save data
%%
data_series=u(xclean,:);
var=x(xclean);


[val,idx_t]=min(abs(T-5*T0/sqrt(g0)));
pos=idx_t;

sz=size(data_series);
dsl=sz(1);
pp=0;
foudata=fftshift(fft(data_series,2^(nextpow2(dsl)+pp),1),1)./dsl;
L=length(foudata(:,1));
hvar=var(2)-var(1);
new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,2^(nextpow2(dsl)+pp))';
% posit=new_omegaxis>=0;
pomega=(new_omegaxis);
pfou=foudata;

	[psor,lsor] = findpeaks(abs(pfou(:,pos)),'SortStr','descend');%sorts the data
	ind1=lsor(1);
	ind2=lsor(2);
	ind3=lsor(3);
	max1=psor(1);
	max2=psor(2);
	max3=psor(3);

	
	if max1>=max2  %%%% this is to refer to the highest first sideband, just in case.
		ind_dist=abs(ind2-ind1);
		fmod= abs(central_frequency-pomega(ind2)); %modulation frequency

	elseif max1<max2
		ind_dist=abs(ind3-ind1);
		fmod= abs(central_frequency-pomega(ind3)); %modulation frequency
	end


	Tmod=1./fmod;  %aproximate modulation period

% 		figure()
% 			subplot(311)
% 			plot(pomega,real(pfou(:,pos)))
% 			hold on
% 			plot(pomega(ind1),max1,'bx')
% 			plot(pomega(ind2),max2,'rx')
% 			plot(pomega(ind3),max3,'bx')
% 			hold off
% 			xlim([-4,4]);
% 			subplot(312)
% 			plot(pomega,abs(pfou(:,pos)))
% 			hold on
% 			plot(pomega(ind1),abs(pfou(ind1,pos)),'gx')
% 			plot(pomega(ind1+ind_dist),abs(pfou(ind1+ind_dist,pos)),'rx')
% 			plot(pomega(ind1-ind_dist),abs(pfou(ind1-ind_dist,pos)),'gx')
% 			plot(pomega(ind1+2*ind_dist),abs(pfou(ind1+2*ind_dist,pos)),'gx')
% 			plot(pomega(ind1-2*ind_dist),abs(pfou(ind1-2*ind_dist,pos)),'gx')
% 			hold off
% 			xlim([-4,4]);
% 			subplot(313)
% 			plot(pomega,mod(unwrap(angle(pfou(:,pos))),2*pi))    
% 			hold on
% 			plot(pomega(ind1),mod(unwrap(angle(pfou(ind1,pos))),2*pi),'gx')    
% 			plot(pomega(ind1+ind_dist),mod(unwrap(angle(pfou(ind1+ind_dist,pos))),2*pi),'gx')    
% 			plot(pomega(ind1-ind_dist),mod(unwrap(angle(pfou(ind1-ind_dist,pos))),2*pi),'gx')    
% 			hold off
% 			xlim([-4,4]);
	
	finalfoudata=pfou;   %/max(foudata(L/2+1:end));
	ModesAmplitudes=zeros(5,length(data_series(1,:)));                                %empty output matrix
% 
	ModesAmplitudes(1,:) = finalfoudata(ind1,:); %central mode
	ModesAmplitudes(2,:) = finalfoudata(ind1+ind_dist,:);   %right first sideband
	ModesAmplitudes(3,:) = finalfoudata(ind1-ind_dist,:);   %left first sideband
	ModesAmplitudes(4,:) = finalfoudata(ind1+2*ind_dist,:); %right second sideband
	ModesAmplitudes(5,:) = finalfoudata(ind1-2*ind_dist,:); %left second sideband

	%%%%%%%%%%%% set ourput array to  a matrix %%%%%%%%%%
% % 	ModesAmplitudes(1,:) = 2*abs(1./(pomega(ind1+1)-pomega(ind1-1))).*trapz(pomega(ind1-1:ind1+1),finalfoudata(ind1-1:ind1+1,:)).*exp(1i.*angle(finalfoudata(ind1,:)));   %right first sideband%finalfoudata(ind1,:); %central mode
% % 	ModesAmplitudes(2,:) = 2*abs(1./(pomega(ind1+ind_dist+1)-pomega(ind1+ind_dist-1))).*trapz(pomega(ind1+ind_dist-1:ind1+ind_dist+1),finalfoudata(ind1+ind_dist-1:ind1+ind_dist+1,:)).*exp(1i.*angle(finalfoudata(ind1+ind_dist,:)));   %right first sideband
% % 	ModesAmplitudes(3,:) = 2*abs(1./(pomega(ind1-ind_dist+1)-pomega(ind1-ind_dist-1))).*trapz(pomega(ind1-ind_dist-1:ind1-ind_dist+1),finalfoudata(ind1-ind_dist-1:ind1-ind_dist+1,:)).*exp(1i.*angle(finalfoudata(ind1-ind_dist,:)));   %right first sideband
% % 	ModesAmplitudes(4,:) = finalfoudata(ind1+2*ind_dist,:); %right second sideband
% 	ModesAmplitudes(5,:) = finalfoudata(ind1-2*ind_dist,:); %left second sideband

	
if any(IC_case==[9])
	car(idx)/abs(ModesAmplitudes(1,1))
	s1(idx)/abs(ModesAmplitudes(2,1)) 
	sm1(idx)/abs(ModesAmplitudes(3,1)) 
end
	Norm_modes_clean =1./((var(end)-var(1))).*trapz(var,abs(u(xclean,1)).^2);
	a0_norm_clean=sqrt(Norm_modes_clean);
%  	a0_norm_clean=a0_norm

	Eta0 = abs(ModesAmplitudes(1,:)).^2./a0_norm_clean.^2;
	Eta1 = abs(ModesAmplitudes(2,:)).^2./a0_norm_clean.^2;
	Etam1 = abs(ModesAmplitudes(3,:)).^2./a0_norm_clean.^2;
	Eta2 = abs(ModesAmplitudes(4,:)).^2./a0_norm_clean.^2;
	Etam2 = abs(ModesAmplitudes(5,:)).^2./a0_norm_clean.^2;
	Gammahat = Eta1 - Etam1;
	Utr = Eta0 + Eta1 + Etam1;
	
	% Eta0 = Eta0./Utr(1) ; Eta1 = Eta1./Utr(1) ;Etam1 = Etam1./Utr(1) ; Eta2 = Eta2./Utr(1)  ;Etam2=Etam2./Utr(1),Utr=Utr./Utr(1);
	% Psi_exp =unwrap( unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:))) - 2.*unwrap(angle(ModesAmplitudes(1,:))));
	Psi_left =unwrap(angle(ModesAmplitudes(3,:)));
	Psi_right=unwrap(angle(ModesAmplitudes(2,:)));
	Psi_main =unwrap(angle(ModesAmplitudes(1,:)));
	Psi_exp =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);
	Psi_exp2 =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(4,:)))+unwrap(angle(ModesAmplitudes(5,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);

	fact=1;
	figure;
	set_latex_plots(groot)
% 	subplot(211)
	hold on
	plot(T+tst, Eta0,'linewidth',1,'color',color_main)
	plot(T+tst, fact*Eta1,'linewidth',1,'color',color_sideband)
	plot(T+tst,fact*Etam1,':','linewidth',1,'color',color_sideband)
	plot(T+tst, fact*Eta2,'linewidth',1,'color',color_2sideband)
	plot(T+tst,fact*Etam2,':','linewidth',1,'color',color_2sideband)
	plot(T+tst, Utr,'--','linewidth',1,'color',color_utr);
	plot([tst,tst],[0,1],':k','linewidth',0.8);
	if any(IC_case==[8,9])
		car(idx)/abs(ModesAmplitudes(1,1))
		s1(idx)/abs(ModesAmplitudes(2,1)) 
		sm1(idx)/abs(ModesAmplitudes(3,1)) 
		plot(t_deb, Eta0d,'-','linewidth',2,'color',color_main)
		plot(t_deb, Eta1d,'-','linewidth',2,'color',color_sideband)
		plot(t_deb, Etam1d,':','linewidth',2,'color',color_sideband)
		plot(t_deb, Eta2d,'-','linewidth',2,'color',color_2sideband)
		plot(t_deb, Etam2d,':','linewidth',2,'color',color_2sideband)
		plot(t_deb, Utrd,'.-','linewidth',2,'color',color_utr);	
	end
	hold off
	xlim([0,Tmax+tst])
	ylim([0,1.02])
	
	
		savefig(fullfile(img_folder,strcat(flm,'_phasespace')));
	saveas(gcf,fullfile(fullfolder,strcat(flm,'_phasespace','.jpeg')));
	
figure()
% abs(new_uspectrum);
% s=pcolor(kclean+k0,T,Uksp2');
% subplot(311)
% hold on
% Uksp2=abs(new_uspectrum);
% % plot(kclean,Uksp2(:,1))
% % plot(kappaxis_deb,Uksp2deb(:,1))
% hold off
% 
% subplot(312)
hold on 
tsearch=t_deb<Tmax+tst
[psor,lsor] = findpeaks(abs(ModesAmplitudes(3,:)),'SortStr','descend');%sorts the data
[psordeb,lsordeb] = findpeaks(abs(ModesAmplitudes_d(3,tsearch)),'SortStr','descend');%sorts the data

plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(1,:)),'--k')
plot((T-T(lsor(1))),abs(ModesAmplitudes(1,:)),'k')
plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(2,:)),'--r')
plot((T-T(lsor(1))),abs(ModesAmplitudes(2,:)),'r')
plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(3,:)),'--b')
plot((T-T(lsor(1))),abs(ModesAmplitudes(3,:)),'b')

abs(ModesAmplitudes_d(2,lsordeb(1)))./abs(ModesAmplitudes(2,lsor(1)))
title('Dashed(Debbie), Filled(Simulation)')
xlim([-700,1200])
% subplot(313)
% plot(T(1:80),abs(ModesAmplitudes_d(3,1:80))./abs(ModesAmplitudes(3,1:80)))
abs(ModesAmplitudes_d(3,1))./abs(ModesAmplitudes(3,1))
%%
if any(IC_case==[1,2,3,8,9])
		
	xclean = abs(x)<=.8*windowwidth; %array cleaning the borders to have better analysis. this is approximate
	data_series=u(xclean,:);
% 	data_series=real(u(xclean,:).*exp(1i.*k0.*x(xclean)'));

	var=x(xclean);
% 	[val,idx]=max(abs(abs));

    pos=80
% 	[ModesAmplitudes]=getsidebands_static_theoretical_SL(data_series,central_frequency,var,pos,1);
	Norm_modes =1./((var(end)-var(1))).*trapz(var,abs(u(xclean,1)).^2);
% 	a0_norm=sqrt(Norm_modes);

	n_wavegauges=length(Tau);
	pos_wg=Tau;
% 		
% 	Eta0 = abs(ModesAmplitudes(1,:)).^2./a0_norm.^2;
% 	Eta1 = abs(ModesAmplitudes(2,:)).^2./a0_norm.^2;
% 	Etam1 = abs(ModesAmplitudes(3,:)).^2./a0_norm.^2;
% 	Eta2 = abs(ModesAmplitudes(4,:)).^2./a0_norm.^2;
% 	Etam2 = abs(ModesAmplitudes(5,:)).^2./a0_norm.^2;
% 	Gammahat = Eta1 - Etam1;
% 	Utr = Eta0 + Eta1 + Etam1;
	
	% Eta0 = Eta0./Utr(1) ; Eta1 = Eta1./Utr(1) ;Etam1 = Etam1./Utr(1) ; Eta2 = Eta2./Utr(1)  ;Etam2=Etam2./Utr(1),Utr=Utr./Utr(1);
	% Psi_exp =unwrap( unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:))) - 2.*unwrap(angle(ModesAmplitudes(1,:))));
	Psi_left =unwrap(angle(ModesAmplitudes(3,:)));
	Psi_right=unwrap(angle(ModesAmplitudes(2,:)));
	Psi_main =unwrap(angle(ModesAmplitudes(1,:)));
	Psi_exp =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);
	Psi_exp2 =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(4,:)))+unwrap(angle(ModesAmplitudes(5,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);


	figure;
	set_latex_plots(groot)
	subplot(211)
	hold on
	plot(T+tst, Eta0,'linewidth',.8,'color',color_main)
	plot(T+tst, Eta1,'linewidth',.8,'color',color_sideband)
	plot(T+tst,Etam1,':','linewidth',.8,'color',color_sideband)
	plot(T+tst, Eta2,'linewidth',.8,'color',color_2sideband)
	plot(T+tst,Etam2,':','linewidth',.8,'color',color_2sideband)
	plot(T+tst, Utr,'--','linewidth',.8,'color',color_utr);
	plot([tst,tst],[0,1],':k','linewidth',0.9);
	if any(IC_case==[8,9])
		car(idx)/abs(ModesAmplitudes(1,1))
		s1(idx)/abs(ModesAmplitudes(2,1)) 
		sm1(idx)/abs(ModesAmplitudes(3,1)) 
		plot(t_deb, Eta0d,'-','linewidth',1.6,'color',color_main)
		plot(t_deb, Eta1d,'-','linewidth',1.6,'color',color_sideband)
		plot(t_deb, Etam1d,':','linewidth',1.6,'color',color_sideband)
		plot(t_deb, Eta2d,'-','linewidth',1.6,'color',color_2sideband)
		plot(t_deb, Etam2d,':','linewidth',1.6,'color',color_2sideband)
		plot(t_deb, Utrd,'.-','linewidth',1.2,'color',color_utr);	
	end
	hold off
	
	legend('$\eta_0$','$\eta_1$','$\eta_{-1}$','$\eta_{2}$','$\eta_{-2}$','$\eta+\eta_1+\eta_{-1}$');
	xlabel('$\tau$','fontsize',Fsz);
	set(gca,'fontsize',Fsz-1)
	ylabel('$\eta_n$','FontSize',Fsz)
	xlim([0,Tmax+tst]);

	subplot(223);
	hold on
	plot((Eta1+Etam1)./ Utr.*cos(Psi_exp./2),(Eta1+Etam1)./Utr.*sin(Psi_exp./2),'-','linewidth',1.5,'color',color_s);
	plot((Eta1(1)+Etam1(1))/Utr(1).*cos(Psi_exp(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Psi_exp(1)/2),'kx','linewidth',1.5);	set(gca,'fontsize',Fsz-1);
	if any(IC_case==[8,9])
		[ d, ix ] = min( abs( t_deb - Tmax ) );%find the critical value index
		t_deb_comp=t_deb<=Tmax+tst;
		plot((Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./ Utrd(t_deb_comp).*cos(Psid_exp(t_deb_comp)./2),(Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./Utrd(t_deb_comp).*sin(Psid_exp(t_deb_comp)./2),'.-','linewidth',2,'color',color_Ns);
		plot((Eta1d(1)+Etam1d(1))/Utrd(1).*cos(Psid_exp(1)/2),(Eta1d(1)+Etam1d(1))/Utr(1)*sin(Psid_exp(1)/2),'kx','linewidth',1.5); set(gca,'fontsize',Fsz-1);
	end
	hold off
% 	axis equal
	xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
	ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
	ylim([-1,1]);
	xlim([-1,1]);

	subplot(224);
	plot(T, Kappa0*Gammahat, ...
		T,abs(P)./N,'--k','linewidth',1.2);
	legend('$\kappa\gamma$','$P/N$');
	% xlabel(mytlabel,'fontsize',15);
	set(gca,'fontsize',Fsz-1);
	xlim([0,Tmax]);
	savefig(fullfile(img_folder,strcat(flm,'_phasespace')));
	saveas(gcf,fullfile(fullfolder,strcat(flm,'_phasespace','.jpeg')));

end
%% Generic plots

%% Conservations plot.
Deltax=x(end)-x(1);
Norm=1./(Deltax).*trapz(x,abs(u(:,:)).^2);
f = plot_conservations_SL(T,Norm,P,Fsz);
savefig(fullfile(img_folder,strcat(flm,'_conservations')));


%%
kclean = 2*pi.*linspace(-1/hx/2,1/hx/2,length(u(xclean,1)))';% kappaxis = kappaxis(1:end-1);   %%%%%%%%%%%%%%%%%%%%%%%%%%check here
new_uspectrum = fftshift(fft(u(xclean,:),[],1),1)./(length(xclean));

figure;
Uksp2 = 10.*log10(abs(new_uspectrum));
UdBth = max(Uksp2)-15;
% UdBth = -19;
Uksp2 = (Uksp2>UdBth).*(Uksp2-UdBth) +  UdBth;
sp=pcolor(kclean+k0,T,Uksp2');
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
set(gca,'YDir','normal','FontSize',Fsz-1);
xlim([1.5*min(kappaxis(kplot))+k0,1.5*max(kappaxis(kplot))+k0]);
ylabel('$\tau$','fontsize',Fsz);
xlabel('$k$[$m^{-1}$]','fontsize',Fsz);
yt = yticks;
xt = xticks;
colormap inferno;
colorbar;
yyaxis right
set(gca, 'ytick', yt/(T0./sqrt(g0)), 'ylim', [0,T(end)./(T0./sqrt(g0))], 'Ycolor', 'k','YDir','normal','FontSize',Fsz-1)
ytickformat('%.1f');
ylabel('$T_{nl}$','fontsize',Fsz-2);
% Pos = get(gca,'Position');
% Yl = get(gca,'ylim');
% axes('Position',Pos);
% plot(k0,T(end),'*r','linewidth',2);
% set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none','xtick',xt)
savefig(fullfile(img_folder,strcat(flm,'_spacepectrum2d')));



figure;
Uksp2l = (abs(new_uspectrum));
sp=pcolor(kclean+k0,T,Uksp2l');
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
set(gca,'YDir','normal','FontSize',Fsz-1);
xlim([1.5*min(kappaxis(kplot))+k0,1.5*max(kappaxis(kplot))+k0]);
ylabel('$\tau$','fontsize',Fsz);
xlabel('$k$[$m^{-1}$]','fontsize',Fsz);
yt = yticks;
xt = xticks;
colormap inferno;
colorbar;
yyaxis right
set(gca, 'ytick', yt/(T0./sqrt(g0)), 'ylim', [0,T(end)./(T0./sqrt(g0))], 'Ycolor', 'k','YDir','normal','FontSize',Fsz-1)
ytickformat('%.1f');
ylabel('$T_{nl}$','fontsize',Fsz-2);
savefig(fullfile(img_folder,strcat(flm,'_spacepectrum2d_linear')));

%%
% spectrum map
figure;
set_latex_plots(groot)
UdBth = -20;
Usp = 10*log10(abs(t_uspectrum.*sqrt(g0)));
Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
sp=pcolor(freqax,x(abs(x(xclean))<=2*pi/Kappa0),Usp(abs(x(xclean))<=2*pi/Kappa0,:));
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
set(gca,'YDir','normal','FontSize',Fsz-1);
xlim([f0-0.5.*2*pi/T0,f0+0.5.*2*pi/T0]);
xlabel('$f$[Hz]','fontsize',Fsz);
ylabel('$X$','fontsize',Fsz);
colormap plasma;
colorbar;
savefig(fullfile(img_folder,strcat(flm,'_timespectrum2d')));



%%
% absolute value evolution
f=figure;
set_latex_plots(groot)
xplot=xclean;
Normalized=0

if Normalized==1
	sp=pcolor(x(xplot ),T,abs(u(xplot,:)./U0)');
else
	sp=pcolor(x(xplot ),T,abs(u(xplot,:).'));
end
set(gca,'YDir','normal','FontSize',Fsz-1);
xlabel('$\xi$','fontsize',Fsz);
ylabel('$\tau$','fontsize',Fsz);
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
yt = yticks;
colormap viridis;
cb=colorbar();
% ylabel(cb, '$|U|$ [m]','Interpreter','latex')
if Normalized==1
	title(cb,'$|U/U_0|$ [m]','Interpreter','latex','FontSize',Fsz-1)
else
	title(cb,'$|U|$ [m]','Interpreter','latex','FontSize',Fsz-1)
end
yyaxis right
set(gca, 'ytick', yt/(T0./sqrt(g0)), 'ylim', [0,T(end)./(T0./sqrt(g0))], 'Ycolor', 'k','YDir','normal','FontSize',Fsz-1)
ytickformat('%.1f');
ylabel('$T_{nl}$','fontsize',Fsz-2);
% set(gca, 'ytick', T./T0, 'ylim', [0,T(end)./T0], 'Ycolor', 'k')
savefig(fullfile(img_folder,strcat(flm,'_envelope2d')));
saveas(gcf,fullfile(fullfolder,strcat(flm,'_enveloped2','.jpeg')));



%%
if any(IC_case==[2,8,9])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Read Debbie's file%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if IC_case==[2]
	% filedebs=fullfile('Debbies_benchmark','IntermediateDepth','HOSM_DataForComparison');
	% if kh0==2.5
	% 	debsdata='Results_modPW_h0p5_02.mat';
	% elseif kh0==5
	% 	debsdata='Results_modPW_h1p0_02.mat';
	% else
	% 	debsdata='Results_modPW_h30p0_01.mat';
	% end
		filedebs=fullfile('Maura_benchmark','sims_toghether');
		if kh0==2.5
			debsdata='shallow05_m3_ampl0012.mat';
		elseif kh0==5
			debsdata='interm1_m3_ampl0012.mat';
		else
			debsdata='deep30_m3_ampl0012.mat';
		end

		debs_vars=load(fullfile(filedebs,debsdata));
		% etadeb=fliplr(debs_vars.eta-mean(debs_vars.eta')');
		etadeb=(debs_vars.eta-mean(debs_vars.eta')');

		t_deb=debs_vars.t;
		x_deb=debs_vars.x-22;
		udeb=hilbert(etadeb);
		% xdebclean=abs(x_deb)<=15;
		xdebclean=abs(x_deb)>=0;

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		%%% Check chebychev type 2 freq filter setup
		% Rs=10; Ns=3;
		% Wn=[3 8]/(12);
		% [b,a] = cheby2(Ns,Rs,Wn);
		% freqz(b,a,600,60)%erase the name to plot.
		fa=length(x_deb)./(x_deb(end)-x_deb(1));
	% fa=1./fa;
	end
figure()
hold on
% udeb_filt=fliplr(hilbert(filter(b,a, etadeb(1,:) ))); 
udeb_filt=(hilbert(bandpass( etadeb,[3 8]./(2.*pi),fa)));
plot(x_deb,abs(udeb(:,idx)),':k','LineWidth',1,'DisplayName','HOSM envelope No-filter')
plot(x_deb,abs(udeb_filt(:,idx)),'-k','LineWidth',1,'DisplayName','HOSM envelope filter')
plot(x,abs(u(:,1)),'b','LineWidth',1.5,'DisplayName','Simulation Filter')
hold off
xlim([-22,22])
xlabel('$x$[m]')
legend()
title('Filtered HOSM test')

%%
Debugdeb=0;
if Debugdeb==1
	%Sanity check. Testing how the time length changes the values of the
	%fourier
	data_series=etadeb(:,1:end-1)';
	var=x_deb(1:end-1);
	sz=size(data_series);
	dsl=sz(1);
	foudata=fftshift(fft(data_series,[],1),1)./dsl;
	hvar=var(2)-var(1);
	new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,dsl)';
	figure()
	subplot(131)
	meshc(new_omegaxis,t_deb,abs(foudata)');
	xlim([2,9])

	data_series=etadeb';
	var=x_deb;
	sz=size(data_series);
	dsl=sz(1);
	foudata=fftshift(fft(data_series,dsl,1),1)./dsl;
	hvar=var(2)-var(1);
	new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,dsl)';
	subplot(132)
	meshc(new_omegaxis,t_deb,abs(foudata)');
	xlim([2,9])


	subplot(133)
	foudata=fftshift(fft(data_series,2.^3.*dsl,1),1)./(2.^3.*dsl);
	new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,(2.^3.*dsl))';
	meshc(new_omegaxis,t_deb,abs(foudata)');
	xlim([2,9])
end
%%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
maxu=max(max(abs((u)')));
u0_prop=U0/maxu;
newmap = viridis;                    %starting map
ncol = size(newmap,1);           %how big is it?
zpos = 1 + floor(u0_prop * ncol);    %2/3 of way through
n=0;
for j=0:n+1 
	newmap(zpos+j,:) = [0.5 0.5 0.5];	
	newmap(zpos-j,:) = [0.5 0.5 0.5];	
end
colormap(newmap);                %activate it
figure()
set_latex_plots(groot)	
subplot(2,2,1)
% s=pcolor(x,T,abs((u+0.5*k0.*(u.*exp(1i*k0.*x')).^2)')); 2nd harmonic
sp=pcolor(x(xclean),T+tst,abs((u(xclean,:))'));
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
colormap viridis;
colormap(newmap);                %activate it
cb=colorbar();
% ylabel(cb, '$|U|$ [m]','Interpreter','latex')
title(cb,'$|U|$ [m]','Interpreter','latex','FontSize',Fsz-1)
xlim([-22,22])
ylim([0,max(T)+tst])
xlabel('$x$[m]')
%%%%%%%% read debbie's data
%find folder, find file, load variables. choose file. make OS agnostic. 
% figure()
subplot(223)
set_latex_plots(groot)	
% title('')
tb_plot=t_deb<T(end)+tst;
sp=pcolor(x_deb,t_deb(tb_plot),abs(ud_rec(:,tb_plot))');
% s=pcolor(x_deb,t_deb,eta);
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
colormap viridis;
colormap(newmap);                %activate it
cb=colorbar();
hold on
Cg_real=Cg./sqrt(g0);
plot([0, x(end)],[0,x(end)/Cg_real],'-r')
hold off
% ylabel(cb, '$|U|$ [m]','Interpreter','latex')
title(cb,'$|U|$ [m]','Interpreter','latex','FontSize',Fsz-1)
xlim([-22,22])
ylim([T(1),T(end)+tst]);
xlabel('$x$[m]')

ax2=subplot(222)
pp=0; %0 if no padding
padding_points=(length(u(xclean,1))+1)*2^pp -1; %Amount of padding.
kclean = 2*pi.*linspace(-1/hx/2,1/hx/2,padding_points)';% kappaxis = kappaxis(1:end-1);   %%%%%%%%%%%%%%%%%%%%%%%%%%check here
new_uspectrum_p = fftshift(fft(u(xclean,:),padding_points,1),1)./length(xclean);
Uksp2=abs(new_uspectrum_p);
sp=pcolor(kclean+k0,T+tst,Uksp2');
% plot(kclean+k0,Uksp2(:,1)');
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
set(gca,'YDir','normal','FontSize',Fsz-1);
xlim([1.5*min(kappaxis(kplot))+k0,1.5*max(kappaxis(kplot))+k0]);
ylabel('$\tau$','fontsize',Fsz);
xlabel('$k$[$1/m$]','fontsize',Fsz);
ylim([0,max(T)+tst])
yt = yticks;
% colormap inferno;
colorbar;
yyaxis right
set(gca, 'ytick', yt/T0, 'ylim', [0,(T(end)+tst)./T0], 'Ycolor', 'k','YDir','normal','FontSize',Fsz-1)
ytickformat('%.1f');
ylabel('$T_{nl}$','fontsize',Fsz-2);
colormap(ax2,'inferno')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data_series=etadeb(:,1:end-1)';
nxdeb=length(x_deb);

pp=0; %0 if no padding
padding_points=(nxdeb+1)*2^pp -1; %Amount of padding.
Lx=padding_points ;%1./length(time_series(:,1)) ;%sqrt length timesample
hxdeb=x_deb(2)-x_deb(1);
kappaxis_deb = 2*pi.*linspace(-1/hxdeb/2,1/hxdeb/2,padding_points)';
new_uspectrum_deb = 2.*fftshift(fft(etadeb,padding_points,1),1)./length(var);

ax2=subplot(224)
% Uksp2deb = 10*log10(abs(new_uspectrum_deb));
% Uksp2deb = (Uksp2deb>UdBth).*(Uksp2deb-UdBth) +  UdBth;
Uksp2deb=abs(new_uspectrum_deb);
sp=pcolor(kappaxis_deb,t_deb,Uksp2deb');
% plot(kappaxis_deb,Uksp2deb(:,idx)')
set(sp, 'EdgeColor', 'None','LineWidth',1.,'MeshStyle','column','FaceColor','interp');
set(gca,'YDir','normal','FontSize',Fsz-1);
xlim([1.5*min(kappaxis(kplot))+k0,1.5*max(kappaxis(kplot))+k0]);
% xlim([0,70]);
ylim([T(1),T(end)+tst]);
ylabel('$\tau$','fontsize',Fsz);
xlabel('$k$[$1/m$]','fontsize',Fsz);
yt = yticks;
colormap(ax2,'inferno')
% colormap viridis;
colorbar;
yyaxis right
set(gca, 'ytick', yt/T0, 'ylim', [0,T(end)./T0], 'Ycolor', 'k','YDir','normal','FontSize',Fsz-1)
ytickformat('%.1f');
ylabel('$T_{nl}$','fontsize',Fsz-2);
savefig(fullfile(img_folder,strcat(flm,'_spacepectrum2d')));
%%
if any(IC_case==[8,9])
	T_plot_max=2000
	t_plot_waves=t_deb(t_deb<T_plot_max)
	nplots=20;
	ar_test=[1:int32(t_plot_waves(end)./20):int32(t_plot_waves(end))];
	tplot=t_deb(ar_test);
	ratio=20/9;
	r1=400;
	figure('position',[20 0 ratio*r1 r1])
	set_latex_plots(groot)

	for j=1:nplots
		subplot(121)
		hold on
		plot(x_deb+t_deb(ar_test(j))*Cg_real,1000.*real(etadeb(:,ar_test(j)))+t_deb(ar_test(j)),'k')
		plot(x_deb+t_deb(ar_test(j))*Cg_real,1000.*abs(hilbert(etadeb(:,ar_test(j))))+t_deb(ar_test(j)),'b')
		plot([x_deb(1)+t_deb(ar_test(end))*Cg_real,x_deb(1)+t_deb(ar_test(end))*Cg_real],[0,t_deb(ar_test(end))],':k')
		plot([x_deb(end),x_deb(end)],[0,t_deb(ar_test(end))],':k')
		hold off
		xlabel('$\xi$')
		ylabel('$\tau$')
		ylim([-5,t_plot_waves(end)+5])
		subplot(122)
		hold on
	% 	plot(kappaxis_deb,10000.*Uksp2deb(:,ar_test(j))-(min(3.*Uksp2deb(:,ar_test(1))))+t_deb(ar_test(j)),'b')
		plot(kappaxis_deb,1000.*Uksp2deb(:,ar_test(j))+t_deb(ar_test(j)),'b')
		hold off
		xlim([k0-2,k0+2]);
		xlabel('$k$')
		ylabel('$\tau$')
		ylim([-5,t_plot_waves(end)+5])
	end
end

%%
Debugdeb=1;
if Debugdeb==1
figure()
subplot(211)
plot(x_deb,etadeb(:,idx),'*')
xlim([-2,2])
ylim([-0.02,0.02])
x_deb(2)-x_deb(1)
subplot(212)
plot(x(xclean), real(u(xclean,1)).*cos(k0.*x(xclean))','*' )
xlim([-2,2])
ylim([-0.02,0.02])
x(2)-x(1)
end
%%



		Psid_left =unwrap(angle(ModesAmplitudes_d(3,:)));
		Psid_right=unwrap(angle(ModesAmplitudes_d(2,:)));
		Psid_main =unwrap(angle(ModesAmplitudes_d(1,:)));
		Psid_2 =unwrap(angle(ModesAmplitudes_d(4,:)));
		Psid_m2 =unwrap(angle(ModesAmplitudes_d(5,:)));
		Psid_3 =unwrap(angle(ModesAmplitudes_d(6,:)));
		Psid_m3 =unwrap(angle(ModesAmplitudes_d(7,:)));

		Psid_exp =unwrap( ...
			unwrap( ...
			unwrap(angle(ModesAmplitudes_d(3,:)))+unwrap(angle(ModesAmplitudes_d(2,:)))...
			) - 2.*unwrap(angle(ModesAmplitudes_d(1,:)))...
			);
		Psid_exp2 =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes_d(4,:)))+unwrap(angle(ModesAmplitudes_d(5,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes_d(1,:)))...
		);

	n_wavegauges=length(t_deb);
	pos_wg=t_deb;
	

	figure;
	set_latex_plots(groot)
	subplot(211)
	title('Debbie 3wave')
	hold on
	plot(t_deb, Eta0d,'linewidth',1.2,'color',color_main)
	plot(t_deb, Eta1d,'linewidth',1.2,'color',color_sideband)
	plot(t_deb, Etam1d,':','linewidth',1.2,'color',color_sideband)
	plot(t_deb, Eta2d,'linewidth',1.2,'color',color_2sideband)
	plot(t_deb, Etam2d,':','linewidth',1.2,'color',color_2sideband)
	plot(t_deb, Utrd,'--','linewidth',1.2,'color',color_utr);	
	hold off
	
	legend('$\eta_0$','$\eta_1$','$\eta_{-1}$','$\eta_{2}$','$\eta_{-2}$','$\eta+\eta_1+\eta_{-1}$');
	xlabel('$\tau$','fontsize',Fsz);
	set(gca,'fontsize',Fsz-1)
	ylabel('$\eta_n$','FontSize',Fsz)
% 	xlim([0,Tmax]);

    [ d, ix ] = min( abs( t_deb - Tmax ) );%find the critical value index
	t_deb_comp=abs(t_deb)<=Tmax;
	
	subplot(223);
	hold on
	plot((Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./ Utrd(t_deb_comp).*cos(Psid_exp(t_deb_comp)./2),(Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./Utrd(t_deb_comp).*sin(Psid_exp(t_deb_comp)./2),'-','linewidth',1.5,'color',color_s);
	plot((Eta1d(1)+Etam1d(1))/Utrd(1).*cos(Psid_exp(1)/2),(Eta1d(1)+Etam1d(1))/Utrd(1)*sin(Psid_exp(1)/2),'kx','linewidth',1.5); set(gca,'fontsize',Fsz-1);
	hold off
% 	axis equal
	xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
	ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
	ylim([-1,1]);
	xlim([-1,1]);

	subplot(224);
	plot(t_deb, Kappa0*Gammahat_deb,'--k','linewidth',1.2);
	legend('$\kappa\gamma$');
	% xlabel(mytlabel,'fontsize',15);
	set(gca,'fontsize',Fsz-1);
	xlim([0,Tmax]);
	
	savefig(fullfile(img_folder,strcat(flm,'debs_phasespace')));
Eta1/Eta0
end



%%
figure()
hold on
subplot(211)
plot(t_deb,max(etadeb))
xlabel('t')
subplot(212)
hold on
plot(t_deb/T0_dim,max(etadeb))
plot([tst/T0_dim,tst/T0_dim],[0,0.05],':k')
hold off
xlabel('t/T_0')
% plot(t_deb,max(abs(hilbert(etadeb))'))
% plot(t_deb,Eta0)
% [psor,lsor] = findpeaks(abs(ModesAmplitudes(1,:)),'MinPeakDistance',30);%sorts the data
% plot(t_deb(lsor),Eta0(lsor),'*r')
hold off
%%
figure()
% abs(new_uspectrum);
% s=pcolor(kclean+k0,T,Uksp2');
% subplot(311)
% hold on
% Uksp2=abs(new_uspectrum);
% % plot(kclean,Uksp2(:,1))
% % plot(kappaxis_deb,Uksp2deb(:,1))
% hold off
% 
% subplot(312)
hold on 
[psor,lsor] = findpeaks(abs(ModesAmplitudes(3,:)),'SortStr','descend');%sorts the data
[psordeb,lsordeb] = findpeaks(abs(ModesAmplitudes_d(3,:)),'SortStr','descend');%sorts the data

plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(1,:)),'--k')
plot((T-T(lsor(1))),abs(ModesAmplitudes(1,:)),'k')
plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(2,:)),'--r')
plot((T-T(lsor(1))),abs(ModesAmplitudes(2,:)),'r')
plot(t_deb-t_deb(lsordeb(1)),abs(ModesAmplitudes_d(3,:)),'--b')
plot((T-T(lsor(1))),abs(ModesAmplitudes(3,:)),'b')

abs(ModesAmplitudes_d(2,lsordeb(1)))./abs(ModesAmplitudes(2,lsor(1)))
title('Dashed(Debbie), Filled(Simulation)')
xlim([-700,1200])
% subplot(313)
% plot(T(1:80),abs(ModesAmplitudes_d(3,1:80))./abs(ModesAmplitudes(3,1:80)))
abs(ModesAmplitudes_d(3,1))./abs(ModesAmplitudes(3,1))


car_error=car(idx)/abs(ModesAmplitudes(1,1))
s1_error=s1(idx)/abs(ModesAmplitudes(2,1))
sm1_error=sm1(idx)/abs(ModesAmplitudes(3,1))
figure()
subplot(211)
hold on
plot(t_deb,abs(ModesAmplitudes_d(2,:)),'--r')
plot((T+tst),s1_error.*abs(ModesAmplitudes(2,:)),'r')
plot(t_deb,abs(ModesAmplitudes_d(3,:)),'--b')
plot((T+tst),sm1_error.*abs(ModesAmplitudes(3,:)),'b')
hold off
xlim([tst,Tmax+tst])
subplot(212)
hold on
plot(t_deb,abs(ModesAmplitudes_d(1,:)),'--r')
plot((T+tst),car_error.*abs(ModesAmplitudes(1,:)),'r')
hold off
title('carrier')
xlim([tst,Tmax+tst])
suptitle('Normalized to the initial sideband error')


Etas1d = s1.^2./U0^2;
Etasm1d = sm1.^2./U0^2;
Etas0d = car.^2./U0^2;
Utrsd = Etas0d + Etas1d + Etasm1d;
psis = movmean(Psid_exp,[Mwin Mwin]);
alp=0.06;



figure('position',[20 0 ratio*r1 1.1*r1])
set_latex_plots(groot)
car_error=car(idx)/abs(ModesAmplitudes(1,1))
s1_error=s1(idx)/abs(ModesAmplitudes(2,1))
sm1_error=sm1(idx)/abs(ModesAmplitudes(3,1))

figure('position',[20 0 ratio*r1 1.1*r1])
set_latex_plots(groot)
subplot(2,10,[1 2 3 4 5 6])
hold on
y=abs(ModesAmplitudes_d(2,:))
y(end)=NaN
patch(t_deb,y,'-','Edgecolor','red','linewidth',1.5,'Edgealpha',alp-0.02);
plot(t_deb,s1,'-r','linewidth',1.6)
plot((T+tst),abs(ModesAmplitudes(2,:)),'--r','linewidth',1.8)
y=abs(ModesAmplitudes_d(3,:))
y(end)=NaN
patch(t_deb,y,'-','Edgecolor','blue','linewidth',1.5,'Edgealpha',alp);
plot(t_deb,sm1,'-b','linewidth',1.6);
plot((T+tst),abs(ModesAmplitudes(3,:)),'--b','linewidth',1.8)
ylabel('Amplitude [m]','FontSize',Fsz)
title('1st sidebands','FontSize',Fsz)
hold off
xlim([tst,Tmax+tst])
subplot(2,10,[11 12 13 14 15 16])
hold on
y=abs(ModesAmplitudes_d(1,:))
y(end)=NaN
patch(t_deb,y,'-','Edgecolor','black','linewidth',1.5,'Edgealpha',alp);
plot(t_deb,car,'-','color',color_main,'linewidth',1.6)
plot((T+tst),abs(ModesAmplitudes(1,:)),'--','color',color_main,'linewidth',1.8)
hold off
ylabel('Amplitude [m]','FontSize',Fsz)
title('Carrier','FontSize',Fsz)
xlabel('Time [s]','FontSize',Fsz)
xlim([tst,Tmax+tst])
subplot(2,10,[ 8 9 10 18 19 20 ])
	hold on
	plot((Eta1+Etam1)./ Utr.*cos(Psi_exp./2),(Eta1+Etam1)./Utr.*sin(Psi_exp./2),'--','linewidth',1.8,'color',color_Ns);
	plot((Eta1(1)+Etam1(1))/Utr(1).*cos(Psi_exp(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Psi_exp(1)/2),'kx','linewidth',1.5);	set(gca,'fontsize',Fsz-1);
	if any(IC_case==[8,9])
		[ d, ix ] = min( abs( t_deb - Tmax ) );%find the critical value index
		t_deb_comp=t_deb<=Tmax+tst;
		y=(Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./Utrd(t_deb_comp).*sin(Psid_exp(t_deb_comp)./2);
		y(end)=NaN
		patch((Eta1d(t_deb_comp)+Etam1d(t_deb_comp))./ Utrd(t_deb_comp).*cos(Psid_exp(t_deb_comp)./2),y,'-','Edgecolor',color_Ns,'linewidth',1.5,'Edgealpha',alp*2);
		plot((Etas1d(t_deb_comp)+Etasm1d(t_deb_comp))./ Utrsd(t_deb_comp).*cos(psis(t_deb_comp)./2),(Etas1d(t_deb_comp)+Etasm1d(t_deb_comp))./Utrsd(t_deb_comp).*sin(psis(t_deb_comp)./2),'-','linewidth',1.6,'color',color_Ns);
		plot((Eta1d(1)+Etam1d(1))/Utrd(1).*cos(Psid_exp(1)/2),(Eta1d(1)+Etam1d(1))/Utr(1)*sin(Psid_exp(1)/2),'kx','linewidth',1.5); 
		set(gca,'fontsize',Fsz-3);
	end
% 	tiledlayout(2,2, 'Padding', 'none', 'TileSpacing', 'compact'); 
	hold off
% 	axis equal
	xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
	ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
	ylim([-1,1]);
	xlim([-1,1]);
% 	axis equal
	savefig(fullfile(img_folder,strcat(flm,'ADHONLS_COMP')));
		saveas(gcf,fullfile(img_folder,strcat(flm,'ADHONLS_COMP_calv.png')));
	
%%
if any(IC_case==[5,7])
f=plot_fourier_sanscarrier_spacelike(u,kappaxis,k0,T,new_uspectrum,windowwidth)
end