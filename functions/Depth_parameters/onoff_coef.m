function [ON_OFF_coef]=onoff_coef(NLS_coef)
	NLS_dispersion = str2double(NLS_coef(1));
	NLS_nonlinear = str2double(NLS_coef(2));
	HO_dispersion = str2double(NLS_coef(3));
	HO_current = str2double(NLS_coef(4));
	HO_asymetry = str2double(NLS_coef(5));
	Hilb_coef = str2double(NLS_coef(6));
	FOD = str2double(NLS_coef(7));
	ON_OFF_coef=[NLS_dispersion,NLS_nonlinear,HO_dispersion,HO_current,HO_asymetry,Hilb_coef,FOD];
	print_eq_coef(NLS_dispersion,NLS_nonlinear,HO_dispersion,HO_current,HO_asymetry,Hilb_coef,FOD);
end