function [h,k,sigma,cg,cp,alpha_hat,bhat_D,alpha_hat3,alpha_hat4,beta_hat21,beta_hat22,mu,muintexp,Hhat3] = depthdependent_HONLS_SpaceLike(omega,kh,ON_OFF_coef)
     %% Parameters for space-like equations
	 
    % input params 
    % omega is the angular frequency in units of g^{1/2}
    % hk is the current value of h(x)k(x)
    % x is the position along the propagation distance
    % 
    % output params
    % h(x) depth
    % k(x) wavenumber
    % sigma = tanh(kh)
    % cg = group velocity
    % cp = phase velocity
    
    %
    % The shoaling is trivial, see Djordjevic&Redekopp ZAMP 1978
    % muintexp is the exp of the integral of mu(x) exp(int(mu))
    %
    % The parameters are defined as in Sedletsky JETP Lett. 97, 180-193 (2003)
    % where the envelope of the surface elevation is used and the expansion
    % to fourth order is performed
    %
    % alpha_hat = group velocity dispersion
    % alpha_hat3 = third order dispersion (=0 in the limit of deep water)
    % alpha4 = fourth order dispersion
    % bhat_D = cubic nonlinear coefficient
    % beta_hat21 = coefficient of |A|^2 D_x A
    % beta_hat22 = coefficient of A D_x |A|^2
    %
    % Andrea ARMAROLI, GAP UniGE, 2019; Alexis GOMEL, GAP UniGE, 2022.
    %%
	
    % auxiliary quantities
    sigma = tanh(kh);
    k = omega.^2./sigma;
    h = kh.*sigma./omega.^2;
    % group and phase speed
    cp = omega./k;
    cg = (sigma + kh.*(1-sigma.^2))/2./omega;
	se=sech(kh);
	co=cosh(kh);
	co2=cosh(2.*kh);
	co3=cosh(3.*kh);
	co4=cosh(4.*kh);
	co5=cosh(5.*kh);
	co6=cosh(6.*kh);
	co7=cosh(7.*kh);
	co8=cosh(8.*kh);
	co9=cosh(9.*kh);
	
%   d_cg=-(kh.*se.^2+sigma).^2./((4.*k.*sigma).^(3/2))+(2.*h.*se.^2-2.*h^2.*k.*se^2.*sigma)./(2.*sqrt(k.*sigma));
	
    % bhat_D in the paper
%   sigma2 = (2*omega*cg).^2 - 4 .* kh .* sigma; %checked
	sigma2 = ((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma); %from Maura's correction
    sigmaq = sigma.^2-1;	%checked
    % mu in the paper
    sigma3 = sigmaq.^2.*kh - sigma.*(sigma.^2-5);
    % NLS quantities
    % beta_hat21''/2 = omega''/Cg^3/2


    lambda_time = cp.*h./sigma.*(1-sigma.^2).*(1-kh.*sigma)-cg.^2/omega; %from Maura's correction (dispersion)
    lambda_time = lambda_time./cg.^3/2;
	alpha_hat=lambda_time.*cg.^3; %deep water dispersion OK.
% 	alpha_hat=alpha_hat;
% 	


	alphaTold = omega./(48.*k.^3.*sigma.^3).*((sigma.^2-1).*(15.*sigma.^4-2.*sigma.^2 + 3).*kh.^3 - ...
          3.*sigma.*(sigma.^2-1).*(3*sigma.^2+1).*kh.^2-...
          3.*sigma.^2.*(sigma.^2-1).*kh - 3.*sigma.^3);
    alphaT = -alphaTold;  
	alpha_time = alphaT./cg.^4 - 2 * alpha_hat.^2.*cg;   
    alpha_hat3=alphaT;
	
	ah41=(1./(384.*k.^4)).*omega;
	alpha_hat4=ah41.*(-15 + 12.*kh.^2 + 46.*kh.^4 + 4.*kh.*(3 - 5.*kh.^2).*coth(kh) + ...
		2.*kh.^2.*(3 + 10.*kh.^2).*coth(kh).^2 + 12.*kh.^3.*coth(kh).^3 - 15.*kh.^4.*coth(kh).^4 +...
		kh.*sigma.*[-12 + 68.*kh.^2 + ...
		3.*kh.*sigma.*(-6 -52.*kh.^2 + 5.*kh.*sigma.*(-4 + 7.*kh.*sigma))]);

		 
		 nu_g=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
	mu_g=2.*sigma./omega.*(2.*omega-k.*cg.*(sigma.^2-1));
	D=-h.*omega.*k.*mu_g./(2.*sigma.*nu_g);
	
	b_D=1./(16*sigma.^4).*(2.*sigma.^6 - 13*sigma.^4 + 12*sigma.^2-9);
	b_corr=-mu_g.^2./(8.*sigma.^2.*nu_g);

	nu_D = -b_D.*k.^2.*omega./cg;
	nu_corr= -b_corr.*k.^2.*omega./cg;
	nu_time=nu_D+nu_corr;
	
	bhat_D=-(omega.*k.^2)./(16.*sigma.^4).*(2.*sigma.^6 - 13.*sigma.^4 + 12.*sigma.^2-9);%space-like nonlinearity OK
    % 4O coefficients
    Q41tilde = 1./(16.*sigma.^5.*sigma2).*((2*sigma.^6 - 11 .* sigma.^4 - 10*sigma.^2 + 27).*sigmaq.^3.*kh.^3 - ...
        sigma.*sigmaq.*(6.*sigma.^8-21.*sigma.^6 +9 .*sigma.^4 - 43.*sigma.^2 + 81).*kh.^2 - ...
        sigma.^3.*(sigma.^2+1).*(2*sigma.^4-7*sigma.^2-27)+...
        sigma.^2.*kh.*(6*sigma.^8-15*sigma.^6-77*sigma.^4+71*sigma.^2 -81));
    Q42tilde = 1./(32.*sigma.^5.*sigma2).*(-(4*sigma.^6 + 5.* sigma.^4 - 10*sigma.^2 + 9).*sigmaq.^3.*kh.^3 + ...
        sigma.*sigmaq.*(12.*sigma.^8 - 45.*sigma.^6 +71 .*sigma.^4 - 15.*sigma.^2 + 9).*kh.^2 + ...
        sigma.^3.*(4*sigma.^6 - 43*sigma.^4 + 118*sigma.^2 + 9) ...
        -sigma.^2.*kh.*(12*sigma.^8-93*sigma.^6+215*sigma.^4-111*sigma.^2+9));
    % Slunyaev correction checked by Maura
    DeltaS = -1./(16.*sigma.^3.*sigma2).*(sigmaq.^4.*(3*sigma.^2+1).*kh.^3 ...
        - sigma.*sigmaq.^2.*(5.*sigma.^4 - 18 *sigma.^2 - 3).*kh.^2 ...
        + sigma.^2.*sigmaq.^2.*(sigma.^2-9).*kh ...
        + sigma.^3.*sigmaq.*(sigma.^2-5));
    % overall |A|^2 D_x A term 
    Q41 = 1./(32.*sigma.^5.*sigma2.^2).*(...
        sigmaq.^5.*(3.*sigma.^6-20.*sigma.^4-21.*sigma.^2 +54).*kh.^5 ...
        - sigma.*sigmaq.^3.*(11.*sigma.^8-99.*sigma.^6-61.*sigma.^4+7.*sigma.^2+270).*kh.^4  ...
        + 2*sigma.^2.*sigmaq.*(7.*sigma.^10-58.*sigma.^8+38.*sigma.^6+52.*sigma.^4-181.*sigma.^2+270).*kh.^3  ...
        - 2.*sigma.^3.*(3.*sigma.^10 + 18.*sigma.^8 - 146.*sigma.^6 - 172.*sigma.^4 + 183.*sigma.^2 - 270).*kh.^2  ...
        - sigma.^4.*(sigma.^8 - 109.*sigma.^6 + 517.*sigma.^4 + 217.*sigma.^2 + 270).*kh ...
        + sigma.^5.*(sigma.^6 - 40.*sigma.^4 + 193.*sigma.^2 + 54) ...
        ) + DeltaS;
	
%     betaT = omega.*k.*Q41;   
    % change to time evolution variable
    % |A|^2 D_t A term
%     beta_hat21 = betaT./(cg.^2) - 4 * alpha_hat.*cg.*bhat_D;  %beta doesn't work for deep limit
	
% 	betaT_tilde = omega.*k.*Q41;   
%     beta_tilde = betaT_tilde./(cg.^2) - 4 * alpha_hat.*cg.*nu_D;  %beta doesn't work for deep limit
    
    % A^2 D_x A* term
%     Q42 = 1./(32.*sigma.^5.*sigma2.^2).*(...
%         - sigmaq.^5.*(3.*sigma.^6 + 7.*sigma.^4-11.*sigma.^2 + 9).*kh.^5 ...
%         + sigma.*sigmaq.^3.*(11.*sigma.^8-48.*sigma.^6 + 66.*sigma.^4+8.*sigma.^2+27).*kh.^4  ...
%         - 2*sigma.^2.*sigmaq.*(7.*sigma.^10-79.*sigma.^8+ 282.*sigma.^6 - 154.*sigma.^4 - sigma.^2+9).*kh.^3  ...
%         + 2.*sigma.^3.*(3.*sigma.^10 - 63.*sigma.^8 + 314.*sigma.^6 - 218.*sigma.^4 + 19.*sigma.^2 + 9).*kh.^2  ...
%         + sigma.^4.*(sigma.^8 + 20.*sigma.^6 - 158.*sigma.^4 - 28.*sigma.^2 - 27).*kh  ...
%         - sigma.^5.*(sigma.^6 - 7.*sigma.^4 + 7.*sigma.^2 - 9) ...
%         ) - DeltaS;

%     gammaT = omega.*k.*Q42;
    % change to time evolution variable
    % A D_t |A|^2 term
%     beta_hat22 = gammaT./cg.^2 - 2 * alpha_hat.*cg.*bhat_D;

% 	gammaT_tilde= omega.*k.*Q42;
% 	gamma_tilde = gammaT_tilde./cg.^2 - 2 * alpha_hat.*cg.*nu_D;

	% shoaling (depth-related loss/amplification)    
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;

	Hhat3=(mu_g.*k./(4.*sigma)).*D;
	
	%NLS
	alpha_hat = alpha_hat.*ON_OFF_coef(1);
	bhat_D = bhat_D.*ON_OFF_coef(2);
	%HONLS
	alpha_hat3 = alpha_hat3.*ON_OFF_coef(3);
	beta_hat21 = omega.*k.*Q41tilde*ON_OFF_coef(4);
    beta_hat22 =  omega.*k.*Q42tilde*ON_OFF_coef(4);
	Hhat3=Hhat3.*ON_OFF_coef(6);%.*tanh(kh./(2.*pi));
	alpha_hat4 = alpha_hat4.*ON_OFF_coef(7);

	
	if ON_OFF_coef(6)==10
 	alpha_hat=omega./(8.*k.^2);
	bhat_D = (omega.*k.^2)./2;
	alpha_hat3=omega./(16.*k.^3);
	beta_hat21=k.^3*8./omega;
	beta_hat22=2*k.^3./omega;
	% shoaling (depth-related loss/amplification)    
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;
	alpha_hat11 = 0;
	end
	
   
end