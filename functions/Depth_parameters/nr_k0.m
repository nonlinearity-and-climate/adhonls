function kfin = nr_k0(k0,w0,h,g,tol)

	function val=nr_k(k0,w0,g,h)
		sigma=tanh(k0*h);
		val=  w0^2/g-k0*sigma;
	end

	function val_prime = nr_k_prime(k0,h)
	   sigma=tanh(k0*h);
	   val_prime= -( sigma+k0*h*(1-sigma^2) );
	end

	min_it=10;
	it=1;
	while it<min_it %DO AT LEAST "min_it" ITERATIONS
		k0 = k0-nr_k(k0,w0,g,h)./nr_k_prime(k0,h);
		it=it+1;
	end
	
	while nr_k(k0,w0,g,h)>tol %Continue until beyond tol
        k0 = k0-nr_k(k0,w0,g,h)./nr_k_prime(k0,h);
		it=it+1;
	end
	
% 	print(k0)
	kfin=k0;
	
	end