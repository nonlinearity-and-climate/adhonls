%%% Sanity chek for the parameter convergence between Dysthe and finite depth.
clear all 
clc
close all
crit= 1.363;
currentFolder = pwd; %set path to current folder.

addpath(genpath(fullfile('E:','sydney 2019','honls-arbitrary-depth-spacelike','functions','plot')));
addpath(fullfile('E:','sydney 2019','honls-arbitrary-depth-spacelike','functions','colormaps'));
% color_disp=[0, 0.4470, 0.7410]; %new default blue dispersion
% color_nl=[0.6350, 0.0780, 0.1840]; %new default red nonlinear
[s_color,e_color]=color_language_beforeafter();
[color_s,color_Ns,color_utr,color_main,color_sideband,color_2sideband]=color_language_3wave();
[color_disp, color_nl]=color_language_NLScoefs();
[color_b21, color_b22,color_h3,color_a3]=color_language_HONLScoefs();
fsz=19;
Fsz=fsz;
axFsz=Fsz-1;
ratio=20/9;
r1=400;

kh=[0.6:0.01:20];
omega=1;
oness=ones(length(kh),1);
sigma = tanh(kh);
k = omega.^2./sigma;
h = kh.*sigma./omega^2;
% group and phase speed
cp = omega./k;
cg = (sigma + kh.*(1-sigma.^2))/2./omega;
x=[1];
FOD=1;
ON_OFF_coef=[1,1,1,1,1,1,1];
FODflag=1;
[h0,k0,sigma0,cg0,cp0,alpha_hat,bhat_D,alpha_hat3,alpha40_hat,beta_hat21,beta_hat22,mu0,muintexp0,Hhat3] = depthdependent_HONLS_SpaceLike(omega,kh,ON_OFF_coef);
[h2,k2,sigma2,cg2,cp2,alpha_hat2,bhat_D2,alpha3_hat2,alpha4_hat2,beta_hat212,beta_hat222,mu,Hhat32,muintexp] = Dysthe_HONLSparameters_SL(omega,kh,x,FOD);

nu_g=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
mu_g=2.*sigma./omega.*(2.*omega-k.*cg.*(sigma.^2-1));
D=-h.*omega.*k.*mu_g./(2.*sigma.*nu_g);
	
b_D_space=omega.*k./(16*sigma.^4).*(2.*sigma.^6 - 13*sigma.^4 + 12*sigma.^2-9);
b_D=1./(16*sigma.^4).*(2.*sigma.^6 - 13*sigma.^4 + 12*sigma.^2-9);
b_corr=-mu_g.^2./(8.*sigma.^2.*nu_g);
q3=b_D+b_corr;
nu_D = -b_D.*k.^2.*omega./cg;

b_D_space_deep=-omega.* ones(1,length(k)).*k(end).^2/2;
figure()
hold on
plot(kh,b_D_space,'-k','LineWidth',1.5)
plot(kh,b_D_space_deep,'--k','LineWidth',1.5)
% plot([crit, crit], [min(cg), max(cg)],':k','LineWidth',1.2)
ylabel('$\hat{B}_D$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off

figure()
hold on
plot(kh,cg,'-k','LineWidth',1.5)
plot(kh,cg2.*oness,'--k')
plot([crit, crit], [min(cg), max(cg)],':k','LineWidth',1.2)
ylabel('$C_g$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))

hold off
xlabel('$kh$','interpreter','latex','FontSize',15)
%%
% hat_alpha= -cg.^3.*(1./(2.*omega.*cg)).*(1-h./(cg.^2).*(1-kh.*sigma).*(1-sigma.^2)); %space-like dispersion.. this is OK.
% hat_alpha_deep=-omega./(8.*k.^2);
% bhat_D=-(omega.*k.^2)./(16.*sigma.^4).*(2.*sigma.^6 - 13.*sigma.^4 + 12.*sigma.^2-9);%space-like nonlinearity.. this is OK.
% bhat_D_deep=(omega.*k.^2)./2;

b_corr=-mu_g.^2./(8.*sigma.^2.*nu_g);
b_hat_meanflow=-b_corr.*k.^2.*omega;%mean flow correction for shallow water under approximation.
b_hat=bhat_D+b_hat_meanflow;

figure('position',[20 0 ratio*(9/9)*r1*1 r1*1])
set_latex_plots(groot)
% subplot(211)
hold on
yyaxis left
plot(kh,alpha_hat,'-','color',color_disp,'DisplayName','$$\hat{\alpha}$$','LineWidth',1.5)
plot(kh,alpha_hat2*oness,'--','color',color_disp,'DisplayName','Deep water')
set(gca,'YColor',color_disp)
set(gca,'YDir','normal','FontSize',Fsz-1);

ax = gca;
ax.FontSize = axFsz; 
ylabel('Dispersion','interpreter','latex','FontSize',Fsz)
yyaxis right
plot(kh,bhat_D,'-.','color',color_nl,'DisplayName','$$\hat{\beta}_D$$','LineWidth',1.5) 
plot(kh,bhat_D2.*oness,'--','color',color_nl,'DisplayName','Deep water')
legend({'$$\hat{\alpha}$$','Deep-water limit','$$\hat{\beta}_D$$','Deep-water limit'},'Interpreter','latex','FontSize',Fsz-2)
set(gca,'YColor',color_nl)
ylim([0,5])
ax = gca;
ax.FontSize = axFsz; 
ylabel('Non-Linearity','interpreter','latex','FontSize',Fsz)
hold off
xlabel('$kh$','FontSize',Fsz)
legend()
xlim([0,10])
title('ADHONLS $3^\mathrm{rd}$ order space-like coefficients','FontSize',Fsz)
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);

set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'sl_disp_nonlin'
%%
figure('position',[20 0 ratio*(9/9)*r1*1. r1*1.])
subplot(121)
hold on
plot(kh,bhat_D,'--','color',color_nl,'DisplayName','$$\hat{\beta_D}$$','LineWidth',1.5) 
plot(kh,b_hat_meanflow,':','color',color_nl,'DisplayName','$$\hat{\beta_{correction}}$$','LineWidth',1.5) 
plot(kh,b_hat,'color',color_nl,'DisplayName','$$\hat{\beta}=\hat{\beta_{D}}+\hat{\beta_{correction}}$$','LineWidth',1.5) 
plot(kh,0.*oness,'--k')
plot([crit, crit], [-2, 2],':k','LineWidth',1.2)
xticks(sort([crit,linspace(0,10,4)]))
xticklabels({'$0$','$1.36$','$3.3$','$6.6$','$10$'})
% xtickformat('%.2f')
xtickangle(45)
legend({'$$\hat{\beta}_D$$','$$\hat{\beta}_{\mathrm{correction}}$$','$$\hat{\beta}$$'},'Interpreter','latex','Location','southeast','FontSize',Fsz-2)
ylim([-2,2])
hold off
ax = gca;
ax.FontSize = axFsz-4; 
xlabel('$kh$','FontSize',Fsz)
xlim([0,10])
title('Non-linear coefficient','FontSize',Fsz)

subplot(122)
hold on
plot([crit, crit], [0, 2],':k','LineWidth',1.2)
plot(kh,abs(b_hat_meanflow)./abs(bhat_D),'color',color_nl,'LineWidth',1.5) 
legend({'','$$\frac{\hat{\beta}_{\mathrm{correction}}}{\hat{\beta}_D}$$'},'Interpreter','latex')
ax = gca;
ax.FontSize = axFsz-4; 
xlabel('$kh$','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),5)]))
% xtickformat('%.2f')
xticklabels({'$0$','$1.36$','$5$','$10$','$15$','$20$'})
xtickangle(45)
hold off
title('Non-linear correction ratio ','FontSize',Fsz)
savefig('disp_v_nonlinear_SL_detail')
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'Nonlin_correction'
exportgraphics(gcf,'Nonlin_correction.png','Resolution',900)

%%

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot(kh,alpha_hat.*sign(alpha_hat2)./alpha_hat2,'-','color',color_disp,'DisplayName',' $sgn ( \alpha^\infty ) \, \alpha (kh)/\alpha^\infty $ ','LineWidth',1.5)
plot(kh,oness.*sign(alpha_hat2),'--','color',color_disp,'DisplayName','$sgn(\alpha^\infty)$')
plot([crit, crit], [min(alpha_hat.*sign(alpha_hat2)./alpha_hat2), max(alpha_hat.*sign(alpha_hat2)./alpha_hat2)],':k','LineWidth',1.2,'DisplayName','Critical $kh$')
ax = gca;
ax.FontSize = axFsz; 
set(gca,'YColor',color_disp)
ylabel('Dispersion','interpreter','latex','FontSize',Fsz)
% set(gca,'YColor','blue')
yyaxis right
plot(kh,oness.*sign(bhat_D2),'--','color',color_nl,'DisplayName', '$sgn(\beta^\infty) \, \beta(kh)/\beta^\infty$')
plot(kh,b_hat./bhat_D2.*sign(bhat_D2),'-','color',color_nl,'DisplayName','$sgn(\beta^\infty)$','LineWidth',1.5)
ax = gca;
ax.FontSize = axFsz; 
ylabel('Non-Linearity','interpreter','latex','FontSize',Fsz)
% set(gca,'YColor','r')
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off
ylim([-2,2])
set(gca,'YColor',color_nl)
title('NLS space-like coeficcients')
legend()
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
savefig('disp_v_nonlinear_SL')

%%

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot(kh,alpha_hat,'-b','DisplayName','Finite depth','LineWidth',1.5)
plot(kh,alpha_hat2.*oness,'--b','DisplayName','Deep water')
plot([crit, crit], [min(alpha_hat), max(alpha_hat)],':k','LineWidth',1.2,'DisplayName','Deep water')
ax = gca;
ax.FontSize = axFsz;
set(gca,'YDir','normal','FontSize',Fsz-1);

ylabel('Dispersion','interpreter','latex','FontSize',Fsz)
set(gca,'YColor',color_disp)
yyaxis right
nu_old = k.^2.*omega./(16*sigma.^4.*cg).*(9 - 10*sigma.^2 + 9*sigma.^4-...
         2.*sigma.^2.*cg.^2./(h-cg.^2).*(4*cp.^2./cg.^2 + 4* cp./cg.*(1-sigma.^2) + h./cg.^2.*(1-sigma.^2).^2));
plot(kh,bhat_D,'--r','DisplayName','$$\beta_D$$','LineWidth',1.5) 
plot(kh,nu_old,':r','DisplayName','$$\beta old$$','LineWidth',1.5) 
plot(kh,bhat_D2.*oness,'--r','DisplayName','Deep water')
plot(kh,b_hat,'-r','DisplayName','$$\beta_D + corrections$$','LineWidth',1.5)
ax = gca;
ax.FontSize = axFsz; 
ylabel('Non-Linearity','interpreter','latex','FontSize',Fsz)
set(gca,'YColor',color_nl)
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off
ylim([-2,2])
legend()
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
title('NLS time-like coeficcients')

% exportgraphics(gcf,'3d_comp_matched.png','Resolution',900)


%%
ix_beta_hat21= detectzerocross(beta_hat21);
ix_beta_hat22= detectzerocross(beta_hat22);


figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21./beta_hat212,'-','color',color_b21,'DisplayName','|A|^2 D_t A','LineWidth',1.8)
plot([crit, crit], [min(beta_hat21), max(beta_hat21)],':k','LineWidth',1.)
ax = gca;
ax.FontSize = axFsz;
ylabel('$\hat\beta_{21}/\hat\beta_{21}^\infty$','interpreter','latex','FontSize',Fsz)
set(gca,'YColor',color_b21)
ylim([-15,1.2])
yyaxis right
plot(kh,beta_hat22./beta_hat222,'-.','color',color_b22,'DisplayName','A D_x |A|^2','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'--k','DisplayName','\beta_{21} (DW)','LineWidth',1)
set(gca,'YColor',color_b22)
ylabel('$\hat\beta_{22}/\hat\beta_{22}^\infty$','interpreter','latex','FontSize',Fsz)
hold off
ylim([-15,1.2])
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),6)]))
axes('Position',[.45 .3 .4 .4])
title('Values around $kh=1.363$','FontSize',Fsz-2)
box on
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21./beta_hat212,'-','color',color_b21,'DisplayName','\hat\beta_{21}','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'-.k','DisplayName','\beta_{21} (DW)','LineWidth',1)
plot([crit, crit], [min(beta_hat21), max(beta_hat21)],':k','LineWidth',1.2)
set(gca,'YColor',color_b21)
% ylim([-15,1.01])
xlim([0.7,1.6])
ylim([-4,1.2])
yyaxis right
% plot(kh,beta_hat222.*oness./beta_hat222,'--','color',color_b22,'DisplayName','$\hat\beta_{22}$ (DW)')
plot(kh,beta_hat22./beta_hat222,'-.','color',color_b22,'DisplayName','$\hat\beta_{22}$','LineWidth',1.8)
set(gca,'YColor',color_b22)
% ylabel('$A D_t |A|^2$','interpreter','latex')
hold off

% ylim([-15,1.01])
xlim([0.9,3.1])
ylim([-4,1.2])

xticks([kh(ix_beta_hat21),crit,kh(ix_beta_hat22)])
ax = gca;
ax.FontSize = axFsz-1;
hold off
% suptitle('Normalized ')
savefig('High order 1 normalized')
% 
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'hatbeta21_22'
%% Add this to appendix, make consistent with other plot
[h,k,sigma,cg,cp,alpha_hat,bhat_D,alpha_hat3,alpha_hat4,beta_hat21s,beta_hat22s,mu,muintexp,Hhat3s] = depthdependent_HONLS_SpaceLike_slun(omega,kh,ON_OFF_coef)
ix_beta_hat21= detectzerocross(beta_hat21s);
ix_beta_hat22= detectzerocross(beta_hat22s);


figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21./beta_hat212,'-','color',color_b21,'DisplayName','|A|^2 D_t A','LineWidth',1.8)
plot([crit, crit], [min(beta_hat21), max(beta_hat21)],':k','LineWidth',1.)
ax = gca;
ax.FontSize = axFsz;
ylabel('$\hat\beta_{21}/\hat\beta_{21}^\infty$','interpreter','latex','FontSize',Fsz)
set(gca,'YColor',color_b21)
ylim([-15,1.2])
yyaxis right
plot(kh,beta_hat22./beta_hat222,'-.','color',color_b22,'DisplayName','A D_x |A|^2','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'--k','DisplayName','\beta_{21} (DW)','LineWidth',1)
set(gca,'YColor',color_b22)
ylabel('$\hat\beta_{22}/\hat\beta_{22}^\infty$','interpreter','latex','FontSize',Fsz)
hold off
ylim([-15,1.2])
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),6)]))
axes('Position',[.45 .3 .4 .4])
title('Values around $kh=1.36$')
box on
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21./beta_hat212,'-','color',color_b21,'DisplayName','\hat\beta_{21}','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'-.k','DisplayName','\beta_{21} (DW)','LineWidth',1)
plot([crit, crit], [min(beta_hat21), max(beta_hat21)],':k','LineWidth',1.2)
set(gca,'YColor',color_b21)
% ylim([-15,1.01])
xlim([0.7,1.6])
ylim([-4,1.2])
yyaxis right
% plot(kh,beta_hat222.*oness./beta_hat222,'--','color',color_b22,'DisplayName','$\hat\beta_{22}$ (DW)')
plot(kh,beta_hat22./beta_hat222,'-.','color',color_b22,'DisplayName','$\hat\beta_{22}$','LineWidth',1.8)
set(gca,'YColor',color_b22)
% ylabel('$A D_t |A|^2$','interpreter','latex')
hold off
% ylim([-15,1.01])
xlim([0.9,3.1])
ylim([-4,1.2])

xticks([kh(ix_beta_hat21),crit,kh(ix_beta_hat22)])
ax = gca;
ax.FontSize = axFsz-2;
hold off
% suptitle('Normalized ')
savefig('High order 3 normalized')
%%

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
yyaxis left
plot(kh,(beta_hat21-beta_hat21s)./beta_hat212,'-','color',color_b21,'DisplayName','|A|^2 D_t A','LineWidth',1.8)
ax = gca;
set(gca,'YDir','normal','FontSize',Fsz-1);
ylabel('$(\hat\beta_{21}-\hat\beta_{21S})/\hat\beta_{21}^\infty$','interpreter','latex','FontSize',Fsz)
% ylim([-15,1.2])
set(gca,'YColor',color_b21)
yticks(sort([linspace(0,0.25,4)]))
ytickformat('%.2f')

yyaxis right
plot(kh,(beta_hat22-beta_hat22s)./beta_hat222,'-.','color',color_b22,'DisplayName','A D_x |A|^2','LineWidth',1.8)
% plot(kh,beta_hat212.*oness./beta_hat212,'-.k','DisplayName','\beta_{21} (DW)','LineWidth',1)
set(gca,'YColor',color_b22)
ylabel('$(\hat\beta_{22}-\hat\beta_{22S})/\hat\beta_{22}^\infty$','interpreter','latex','FontSize',Fsz)
hold off
% ylim([-15,1.2])
yticks(sort([linspace(-1.5,0,4)]))
ytickformat('%.2f')

xlabel('$kh$','interpreter','latex','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),6)]))
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'hatbeta21_22_slundev'
% axes('Position',[.45 .2 .4 .4])
% title('Values around $kh=1.36$')
%%

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21s./beta_hat212,'-','color',color_b21,'DisplayName','|A|^2 D_t A','LineWidth',1.8)
plot([crit, crit], [min(beta_hat21s), max(beta_hat21s)],':k','LineWidth',1.)
ax = gca;
ax.FontSize = axFsz;
ylabel('$\hat\beta_{21S}/\hat\beta_{21}^\infty$','interpreter','latex','FontSize',Fsz)
set(gca,'YColor',color_b21)
ylim([-15,1.2])
yyaxis right
plot(kh,beta_hat22s./beta_hat222,'-.','color',color_b22,'DisplayName','A D_x |A|^2','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'--k','DisplayName','\beta_{21} (DW)','LineWidth',1)
set(gca,'YColor',color_b22)
ylabel('$\hat\beta_{22S}/\hat\beta_{22}^\infty$','interpreter','latex','FontSize',Fsz)
hold off
ylim([-15,1.2])
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),6)]))
axes('Position',[.45 .3 .4 .4])
title('Values around $kh=1.36$')
box on
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_hat21s./beta_hat212,'-','color',color_b21,'DisplayName','\hat\beta_{21}','LineWidth',1.8)
plot(kh,beta_hat212.*oness./beta_hat212,'-.k','DisplayName','\beta_{21} (DW)','LineWidth',1)
plot([crit, crit], [min(beta_hat21s), max(beta_hat21s)],':k','LineWidth',1.2)
set(gca,'YColor',color_b21)
% ylim([-15,1.01])
xlim([0.7,1.6])
ylim([-4,1.2])
yyaxis right
% plot(kh,beta_hat222.*oness./beta_hat222,'--','color',color_b22,'DisplayName','$\hat\beta_{22}$ (DW)')
plot(kh,beta_hat22s./beta_hat222,'-.','color',color_b22,'DisplayName','$\hat\beta_{22}$','LineWidth',1.8)
set(gca,'YColor',color_b22)
% ylabel('$A D_t |A|^2$','interpreter','latex')
hold off
% ylim([-15,1.01])
xlim([0.9,3.1])
ylim([-4,1.2])

xticks([kh(ix_beta_hat21),crit,kh(ix_beta_hat22)])
ax = gca;
ax.FontSize = axFsz-2;
hold off
% suptitle('Normalized ')
savefig('High order 4 normalized')
% axes('Position',[.45 .18 .4 .4])
% box on
% hold on
% yyaxis left
% plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
% 
% plot(kh,beta,'-b','DisplayName','\beta_{21}','LineWidth',1.5)
% plot(kh,beta2.*oness,'--b','DisplayName','\beta_{21} (DW)')
% plot([crit, crit], [min(beta), max(beta)],':k','LineWidth',1.2)
% 
% % ylabel('$|A|^2 D_t A$','interpreter','latex')
% set(gca,'YColor','blue')
% ylim([-15,10])
% xlim([0.7,1.6])
% yyaxis right
% plot(kh,gamma2.*oness,'--r','DisplayName','$\beta_{22}$ (DW)')
% plot(kh,gamma,'-r','DisplayName','$\beta_{22}$','LineWidth',1.5)
% set(gca,'YColor','red')
% % ylabel('$A D_t |A|^2$','interpreter','latex')
% hold off
% ylim([-15,10])
% xlim([min([kh(ix_beta),crit,kh(ix_gamma)])-0.15,1.8])
% try
% xticks(sort([kh(ix_beta),crit,kh(ix_gamma)]));
% end
% hold off
%%
% sigma2 = (2.*omega.*cg).^2 - 4 .* kh .* sigma;
% nu_paper_sigma=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
% nu_dw=1-4.*kh;
% figure()
% hold on
% plot(kh,sigma2)
% plot(kh,nu_paper_sigma)	
% plot(kh,nu_dw)
% xlabel('$kh$','interpreter','latex','FontSize',15)

%%
val_cross= detectzerocross(alpha_hat3);

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
% yyaxis left
% plot(kh,alpha_hat3,'-k','LineWidth',1.5)

plot(kh,alpha_hat3./alpha3_hat2,'-','color',color_a3,'LineWidth',1.5)
plot(kh,oness,'--','color',color_a3)
plot([crit, crit], [min(alpha_hat3./alpha3_hat2), max(alpha_hat3./alpha3_hat2)],':k','LineWidth',1.2)
% set(gca,'YColor','blue')
% ylim([-15,10])
set(gca,'YColor',color_a3)

xticks(sort([kh(val_cross),1.4,linspace(0,max(kh),6)]))
hold off
ax = gca;
ax.FontSize = axFsz-2;
ylabel('Third order dispersion $\hat\alpha_3/\hat\alpha_3^\infty$','interpreter','latex','FontSize',Fsz)

xlabel('$kh$','interpreter','latex','FontSize',Fsz)
ylim([0,2.5])
set(gca,'YDir','normal','FontSize',Fsz-4);
set(gca,'XDir','normal','FontSize',Fsz-4);
xtickangle(45)

set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'Third_order_SL'
%%
val_cross= detectzerocross(alpha40_hat);

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
plot(kh,alpha40_hat./alpha40_hat(end),'-k','LineWidth',1.5)
plot([crit, crit], [-0.5,3.5],':k','LineWidth',1.2)
plot(kh,oness,'--k')

hold off
xticks(sort([kh(val_cross),linspace(0,max(kh),6)]))
ax = gca;
ax.FontSize = axFsz-2;
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
ylabel('Fourth order dispersion $\hat\alpha_4/\hat\alpha_4^\infty$','interpreter','latex','FontSize',Fsz)
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);
% xtickangle(45)

set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'fourth_order_SL'
%%
% val_cross= detectzerocross(alpha);

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
% yyaxis left
% plot(kh,D,'-b','LineWidth',1.5,'DisplayName','D ')
plot(kh,D./(omega/2),'-b','LineWidth',1.5,'DisplayName','D ','LineWidth',1.5)
% plot(kh,omega.*oness./2,'--b','DisplayName','\omega/2')
plot(kh,oness,'--b','DisplayName','\omega/2','LineWidth',1.5)
ax = gca;
ax.FontSize = axFsz-2;
% plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('$\frac{D}{\omega/2}$','interpreter','latex','FontSize',Fsz)
% set(gca,'YColor','blue')
% plot([crit, crit], [min(D)-0.5, max(D)],':k','LineWidth',1.2)
plot([crit, crit], [0.9, max(D./(omega/2))],':k','LineWidth',1.2)

% legend()
% ylim([0.45,1])
ylim([0.9, max(D./(omega/2))])
xlim([0,10])
% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',Fsz)



%% D hilbert term

D_hilb_num=2+(1-sigma.^2).*cg./cp;
D_hilb_den=kh-sigma.*cg.^2./(cp.^2);
D_hilb_general=kh.*(D_hilb_num./D_hilb_den).*omega./(4.*sigma);
% D_hilb_2=(mu_g.*k./(4.*sigma.*cg.^2)).*D_hilb_2;
% val_cross= detectzerocross(alpha);

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
% yyaxis left
% plot(kh,D_hilb_2,'--b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2)_2 ')
plot(kh,Hhat3./Hhat32,'-','color',color_h3,'LineWidth',1.5,'DisplayName','$\hat H_3/(2k^3/\omega)$')
plot(kh,oness,'--','color',color_h3,'LineWidth',1.5,'DisplayName','$2k^3/\omega$')
% plot(kh,log(Dys_hilb_gen),'-b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2) ')
% plot(kh,log(2.*k.^3./omega),'--k','LineWidth',1.5,'DisplayName','2k^3/\omega')
plot([crit, crit], [min(Hhat3), max(Hhat3)],':k','LineWidth',1.2,'DisplayName','critical')
ax = gca;
ax.FontSize = axFsz-2;
ylabel('$\hat H_3/(2k^3/\omega)$','interpreter','latex','FontSize',Fsz)
% legend()
set(gca,'YColor',color_h3)

% xlim([0.5,5])
hold off
ylim([0.9,2.3])

xlabel('$kh$','interpreter','latex','FontSize',Fsz)
% set(gca, 'YScale', 'log')
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);
% xtickangle(45)

set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'hatH3'
%% Easy approximation

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
x=-10:0.01:10;
hold on
plot(x,1./(tanh(x)),'-k','LineWidth',1.5,'DisplayName','$\coth(kh)$')
plot(x,1./x+sign(x),'--k','LineWidth',1.5,'DisplayName','$1/kh+\mathrm{sgn}(k)$')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
ylim([-5,5])
ax = gca;
ax.FontSize = axFsz-2;
legend('FontSize',Fsz-2)
hold off
set(gca,'YDir','normal','FontSize',Fsz-1);
set(gca,'XDir','normal','FontSize',Fsz-1);
% xtickangle(45)

set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'simple_approx'
%% easyer aprox

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
x=-10:0.01:10;
fnew=zeros(1,length(x));
for j=1:length(x)
	bla=1-abs(x(j))./2;
	if bla>0
	fnew(j)=bla;
	end
end
hold on
plot(x,1./(tanh(x)),'-r','LineWidth',2.1,'DisplayName','$\coth{(kh)}$')
plot(x,1./x+sign(x),'-b','LineWidth',1.9,'DisplayName','$1/kh+\mathrm{sgn}(k)$')
plot(x,1./x-x./(x.^2+1)+sign(x),'-k','LineWidth',1.9,'DisplayName','$1/kh+kh/(kh^2+1)+\mathrm{sgn}(k)$')

% plot(x,1./(x.*(x.^2+1))+sign(x),'-k','LineWidth',1.9,'DisplayName','$1/((kh)^2+1)$')
plot(x,exp(-abs(x))./x+sign(x),'--k','LineWidth',1.9,'DisplayName','$e^{-|kh|}/kh+\mathrm{sgn}(k)$')

% plot(x,(fnew)./x+sign(x),':k','LineWidth',1.5,'DisplayName','fnew+sgn(k)')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
% ylabel('Approximation / $\coth{(kh)}$','interpreter','latex','FontSize',Fsz)
% xlim
ax = gca;
ax.FontSize = Fsz-2;
ylim([0.95,3])
legend('Fontsize',Fsz-3)
hold off
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'Other_aprox_zoom'

%%

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
% plot(x,1./(tanh(x)),'-r','LineWidth',1.5,'DisplayName','coth(kh)')
plot(x,abs((1./x+sign(x)).*(tanh(x))),'-b','LineWidth',1.9,'DisplayName','$1/kh+\mathrm{sgn}(k)$')
plot(x,abs((1./(x.*(x.^2+1))+sign(x)).*(tanh(x))),'-k','LineWidth',1.9,'DisplayName','$1/kh+kh/(kh^2+1)+\mathrm{sgn}(k)$')

plot(x,abs((exp(-abs(x))./x+sign(x)).*(tanh(x))),'--k','LineWidth',1.9,'DisplayName','$e^{-|kh|}/kh+\mathrm{sgn}(k)$')
% plot(x,abs(((fnew)./x+sign(x)).*(tanh(x) )),':k','LineWidth',1.5,'DisplayName','fnew+sgn(k)')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
ylabel('Approximation / $\coth{(kh)}$','interpreter','latex','FontSize',Fsz)
xlim([0,10])

ax = gca;
ax.FontSize = Fsz-2;
ylim([1,1.6])
legend('Fontsize',Fsz-3)
hold off
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)])
print  -dpdf -painters 'Other_aprox'
%% easyer aprox

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
x=-10:0.01:10;
fnew=zeros(1,length(x));
for j=1:length(x)
	bla=1-abs(x(j))./2;
	if bla>0
	fnew(j)=bla;
	end
end
hold on

plot(x,exp(-abs(x)),'--k','LineWidth',1.5,'DisplayName','exp(-abs(kh))')
plot(x,1./(x.^2+1),'-k','LineWidth',1.5,'DisplayName','1/(x^2+1)')

plot(x,(fnew),':k','LineWidth',1.5,'DisplayName','f piecewise')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')
xlabel('$kh$','interpreter','latex','FontSize',15)
% ylim([-5,5])
xlim([0,10])
ax = gca;
ax.FontSize = axFsz-2;
legend()
hold off

%% stokes drift test. 




kh_sd=[0.6,1.3,1.4,2]
eta0=0.001.*exp(-x.^2).*cos(k0.*x);
U0=hilbert(eta0);

figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
subplot(121)

hold on
for i=1:length(kh_sd)
	sig_sd=tanh(kh_sd(i));
	k0=omega/sig_sd;
h0=kh_sd(i)/k0;
z=linspace(0,-h0,100);
us=omega.*k0.*0.5.*cosh(2.*k0.*(h0.*z))/(2.*(sinh(k0.*h0)).^2).*abs(U0').^2;
% plot(x,abs(U0))
% plot(x(int32(length(x)/2)).*ones(length(z),1),z,'--k')
plot(us(int32(length(x)/2),:),z)
end
ylabel('$z$')
xlabel('$u_{sd}$')
hold off
% xlim([-0.001,0.5])
subplot(122)
hold on
for i=1:length(kh_sd)
	sig_sd=tanh(kh_sd(i));
	k0=omega/sig_sd;
h0=	kh_sd(i)/k0;
z=linspace(0,-h0,100);

cp_sd=omega/k0;
cg_sd = (sig_sd + kh_sd(i).*(1-sig_sd.^2))/2./omega;
pu2=(abs(U0(2:end)').^2-abs(U0(1:end-1)').^2)./(x(2:end)-x(1:end-1))';
ws=-omega.*0.25.*(1+cg_sd./cp_sd).*k0.*0.5.*sinh(2.*k0.*(h0.*z))./(2.*(sinh(k0.*h0)).^2).*pu2.*k0.*0.001;


% plot(ws(int32(length(x)/2),:),z,'-')
plot(ws(int32(length(x)/2)-100,:),z,'--')
% plot(ws(int32(length(x)/2)+100,:),z,':')

end
hold off

ylabel('$z$')
xlabel('$w_{sd}$')
%%
% val_cross= detectzerocross(alpha4_hat);
% figure()
% hold on
% plot(kh,alpha4_hat,'-k','LineWidth',1.5)
% plot(kh,0.*oness,'--k')
% plot([crit, crit], [min(alpha4_hat), max(alpha4_hat)],':k','LineWidth',1.2)
% xticks(sort([kh(val_cross),linspace(0,max(kh),6)]))
% ylabel('$\alpha_4$','interpreter','latex','FontSize',15)
% xlabel('$kh$','interpreter','latex','FontSize',13)

%%
function zerocross = detectzerocross(x)
	test_cross=x(1:end-1).*x(2:end);
	zerocross= find(test_cross<0);
end