function [u4,err] = DV4ORK34_spacelike_shiftAD(ht,u,kx,h,alpha_hat,bhat_D,alpha_hat3,alpha_hat4,beta_hat21,beta_hat22,Hhat3,kh0)
% adaptive embedded RK4(3) method for solving the variable depth HONLS in the Interaction Picture
% INPUT
% hz: current step size
% u: complex field
% kx: frequency bins
% alpha_hat, 1, 2 are the values of lambda at x, x+hx/2, x + hx
% mn0, 1, 2 are the values of bhat_D at x, x+hx/2, x + hx
% alpha_hat3, 1, 2 are the values of alpha (TOD) at x, x+hx/2, x + hx
% alpha4_hat0, 1, 2 are the values of alpha (FOD) at x, x+hx/2, x + hx 
% beta_hat21, 1, 2 are the values of beta (|A|^2 A_t) at x, x+hx/2, x + hx
% gamma0, 1, 2 are the values of beta (A^2 A*_t) at x, x+hx/2, x + hx
% 
% We limit ourselves to the Djordjevic Redekopp shoaling
% muintexp0, 1, 2 are the values of exponential of the integral of shoaling at x, x+hx/2, x + hx
% shoalflag allows to switch shoaling off (it could be useful to compare to fibres)
% 
% BEWARE the IP integrating factor now involves an integral of lambda in
% the expontential and the multiplication by a factor on account of
% shoaling.
% 
% OUTPUT: function value at z + hz, correcting hz0 accordingly
% 
% Andrea Armaroli 26.02.19 
% Coefficients come from Sedletsky work. No nonlocality is present in the
% finite depth case
% Added hilbert term
% 02.04.19 Included 4OD to suppress the resonant-radiation
%
persistent k5
n = size(kx,1);
mask = ones(n,1);
mask(round(6*n/14:8*n/12-1)) = 0;


%     integrating factor x -> x + hx/2
    phaselin1 = exp(-1i.*(kx.^2.*(alpha_hat)' + kx.^3.*(alpha_hat3)'+ kx.^4.*(alpha_hat4)'  ).*ht./2);%omega.^4.*(alpha40 + alpha41)/2 ).*hx/2
    
%     integrating factor x+ hx/2 -> x + hx
    phaselin2 = exp(-1i.*(kx.^2.*(alpha_hat)' + kx.^3.*(alpha_hat3)'+ kx.^4.*(alpha_hat4)'  ).*ht./2);
    
uIP = ifft(ifftshift((phaselin1.*fftshift(fft(u).*mask))));
if isempty(k5)
    Ncurr = nonlinear(u,kx, bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh0);
    k1 = ifft(ifftshift(phaselin1.*fftshift(fft(Ncurr))));
else
    k1 = ifft(ifftshift(phaselin1.*fftshift(fft(k5))));
end

	
k2 = nonlinear(uIP + ht/2.*k1 ,kx, bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh0);
k3 = nonlinear(uIP + ht/2.*k2 ,kx,  bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh0);
u2 = ifft(ifftshift(phaselin2.*fftshift(fft(uIP + ht.*k3))));
k4 = nonlinear(u2, kx, bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh0);
ns = ifft(ifftshift(phaselin2.*fftshift(fft(uIP + ht./6.*(k1 + 2.*k2 + 2.*k3)))));
u4 = ns + ht./6.*k4;
k5 = nonlinear(u4, kx, bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh0);
u3 = ns + ht./30.*(2.*k4+3.*k5);
err = norm(u3-u4);
end 

function [uNL] = nonlinear(u, kx, bhat_D, beta_hat21, beta_hat22,Hhat3,h,kh)
% uNL3O = bhat_D*abs(u).^2.*u;
% uNL4O = beta.*abs(u).^2.*fft(-1i.*kx.*ifft(u)) + beta_hat22.*u.^2.*fft(-1i.*kx.*ifft(conj(u)))+Hhat3.*1i.*u.*fft(abs(kx)./(tanh(h.*kx)).*ifft(abs(u).^2)) ;
% uNL4O = beta_hat21.*abs(u).^2.*fft(-1i.*kx.*ifft(u)) + beta_hat22.*u.^2.*fft(-1i.*kx.*ifft(conj(u)))-Hhat3.*1i.*u.*fft((kx-kh/h)./(tanh(h.*(kx)-kh)).*ifft(abs(u).^2)) ;
% uNL4O = beta_hat21.*abs(u).^2.*fft(-1i.*kx.*ifft(u)) + beta_hat22.*u.^2.*fft(-1i.*kx.*ifft(conj(u)))  +  Hhat3.*1i.*u.*fft((kx)./(tanh(h.*(kx)+kh)).*ifft(abs(u).^2)) ;
% uNL4O = beta_hat21.*abs(u).^2.*ifft(ifftshift(-1i.*kx.*fftshift(fft(u)))) + beta_hat22.*u.^2.*ifft(ifftshift(-1i.*kx.*fftshift(fft(conj(u)))))  +  Hhat3.*1i.*u.*ifft(ifftshift((kx./h)./(tanh(h.*(kx))).*fftshift(fft(abs(u).^2)))) ;
% uNL4O = beta_hat21.*abs(u).^2.*ifft(ifftshift(-1i.*kx.*fftshift(fft(u)))) + beta_hat22.*u.^2.*ifft(ifftshift(-1i.*kx.*fftshift(fft(conj(u)))))  +  Hhat3.*1i.*u.*ifft(ifftshift((kx)./(tanh(h.*(kx))).*fftshift(fft(abs(u).^2)))) ;
hat_uNL4O = beta_hat21.*conj(u).*ifft(ifftshift(-1i.*kx.*fftshift(fft(u)))) + beta_hat22.*u.*ifft(ifftshift(-1i.*kx.*fftshift(fft(conj(u)))))  +  Hhat3.*1i.*ifft(ifftshift((kx)./(tanh(h.*(kx))).*fftshift(fft(abs(u).^2)))) ;
uNL = -u.*(1i.*bhat_D.*abs(u).^2 - hat_uNL4O);
end


%keeping the u in the nonlinear part outside might make the code more
%stable. 