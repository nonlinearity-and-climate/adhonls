function [ModesIndex, Tmod]=getsidebands_pos(time_series,central_frequency,time,Debug)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code searches for the first and second sidebands of a time series. (array)
% Inputs: 
%         - time is the time array for the time_series.
%         - Time series array, is the array to extract the sideband
%         information from
%         - Aproximate central frequency
%         - Debug option: 1 to print debug plot, 0 if not.

% Outputs:
%         - ModesAmplitudes: Is an array with the complex value of the fft
%         main modes, up to the 2nd sidebands
%         - ModesPhases: Is an array with the Phases of the fft
%         main modes, up to the 2nd sidebands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
window=0;
if window==1 %% Optional Tukey windowing + padding.
    %%windowing parameters
    tukeyr=0.01;
    padding_points=0; %Amount of padding.

    Nx=length(time_series(1,:));  %space vector lenght
	Nx1=Nx;
    ht=time(2)-time(1);           %sampling frequency
    tukeylen=length(time);
    length(time_series);
    win=tukeywin(tukeylen,tukeyr);
    if Debug==1
        figure()
        plot(time,time_series(:,1).*win)
        hold on
        plot(time,max(time_series(:,1)).*win,'--k')
        hold off
    end
    
    if Debug==1
    Nx_win=length(win);  %space vector lenght
    ht_win=time(2)-time(1);           %sampling frequency
    foudata_win=fftshift(ifft(win,[],1),1);                 %fourier of the data
    L_win=length(foudata_win);                                          %time series lenght
    omegaxis_win = linspace(-1/ht_win/2,1/ht_win/2,L_win+1); 
    omegaxis_win = omegaxis_win(1:end-1);     
       figure('name','tukey info')
       subplot(2,1,1)
       plot(omegaxis_win,abs(foudata_win))
       title('window fourier')
       xlabel('Hz')
       subplot(2,1,2)
       plot(omegaxis_win,(angle(foudata_win))./pi)
       title('window phase')
       xlabel('Hz')
    end

    time_series=time_series.*tukeywin(tukeylen,tukeyr);  
%     newtime=zeros(tukeylen+padding_points,1);
%     newseries=zeros(tukeylen+padding_points,Nx);
%     newtime=time(1)-ht*padding_points/2:ht:time(end)+ht*padding_points/2;
%     newseries(round(padding_points/2)+1:tukeylen+round(padding_points/2),:)=time_series;%this way it keeps the right times.
%     % figure()
%     % plot(newtime,newseries(:,1))
%     time_series=newseries;
%     time=newtime;
end

Nx=length(time_series(1,:))   %space vector lenght
ht=time(2)-time(1) ;         %sampling frequency
Lt=length(time);

padding_points=Lt+2.^13; %Amount of padding. %Lt give 0 added padding.
Lx=2.*padding_points./Lt;%1./length(time_series(:,1)) ;%sqrt length timesample

foudata=fftshift(ifft(time_series,padding_points,1),1);
% Lx=sqrt(length(time_series(:,1)).*2 ) ;%sqrt length timesample
L=length(foudata);                                          %time series lenght

ModesAmplitudes=zeros(5,Nx);                                %empty output matrix
omegaxis = linspace(-1/ht/2,1/ht/2,L+1); 
omegaxis = omegaxis(1:end-1);                               %Frequency axis, in real frequency units.
%detect noise (very small numbers (eps)) and ignore them
threshold = max(abs(foudata))/10; %tolerance threshold
%remove small values:
% foudata(abs(foudata) < threshold) = 0; %maskout values that are below the threshold

%temporal variables to get the indices for the first signal.
new_omegaxis=omegaxis(L/2+1:end);  %right side of the freq axis
data_gauge=1;
newfoudata=foudata(L/2+1:end,data_gauge); %right side of the fourier data, for the first gauge, normalized
   

if Debug==1
    ind_pos_debug=1;%To debug different positions.
    newfoudata=foudata(L/2+1:end,ind_pos_debug); %right side of the fourier data, for the first gauge, normalized
    figure()
    subplot(311)
    plot(new_omegaxis,real(newfoudata))
end
ModesIndex=zeros(5,Nx);                         %empty output matrix

[psor,lsor] = findpeaks(abs(newfoudata),'SortStr','descend');%sorts the data
ind1=lsor(1);
ind2=lsor(2);
ind3=lsor(3);
max1=psor(1);
max2=psor(2);
max3=psor(3);

if max1>=max2  %%%% this is to refer to the highest first sideband, just in case.
    ind_dist=abs(ind2-ind1);
    fmod= abs(central_frequency-new_omegaxis(ind2)); %modulation frequency
    
elseif max1<max2
    ind_dist=abs(ind3-ind1);
    fmod= abs(central_frequency-new_omegaxis(ind3)); %modulation frequency
end
Tmod=1./fmod;                                 %aproximate modulation period

if Debug==1
    ind_pos_debug=1;%To debug different positions.
    newfoudata=foudata(L/2+1:end,ind_pos_debug); %right side of the fourier data, for the first gauge, normalized
    figure()
    subplot(311)
    plot(new_omegaxis,real(newfoudata))
    hold on
    plot(new_omegaxis(ind1),max1,'bx')
    plot(new_omegaxis(ind2),max2,'bx')
    plot(new_omegaxis(ind3),max3,'bx')
    hold off
    xlim([0,4]);
    subplot(312)
    plot(new_omegaxis,abs(newfoudata))
    hold on
    plot(new_omegaxis(ind1),abs(newfoudata(ind1)),'gx')
    plot(new_omegaxis(ind1+ind_dist),abs(newfoudata(ind1+ind_dist)),'gx')
    plot(new_omegaxis(ind1-ind_dist),abs(newfoudata(ind1-ind_dist)),'gx')
    plot(new_omegaxis(ind1+2*ind_dist),abs(newfoudata(ind1+2*ind_dist)),'gx')
    plot(new_omegaxis(ind1-2*ind_dist),abs(newfoudata(ind1-2*ind_dist)),'gx')
    hold off
    xlim([0,4]);
    subplot(313)
    plot(new_omegaxis,mod(unwrap(angle(newfoudata)),2*pi))    
    hold on
    plot(new_omegaxis(ind1),mod(unwrap(angle(newfoudata(ind1))),2*pi),'gx')    
    plot(new_omegaxis(ind1+ind_dist),mod(unwrap(angle(newfoudata(ind1+ind_dist))),2*pi),'gx')    
    plot(new_omegaxis(ind1-ind_dist),mod(unwrap(angle(newfoudata(ind1-ind_dist))),2*pi),'gx')    
    hold off
    xlim([0,4]);
end

finalfoudata=foudata(L/2+1:end,:);   %/max(foudata(L/2+1:end));


%%%%%%%%%%%% set ourput array to  a matrix %%%%%%%%%%
ModesIndex(1,:) = ind1; %central mode
ModesIndex(2,:) = ind1+ind_dist;   %right first sideband
ModesIndex(3,:) = ind1-ind_dist;   %left first sideband
ModesIndex(4,:) = ind1+2*ind_dist; %right second sideband
ModesIndex(5,:) = ind1-2*ind_dist; %left second sideband

end

%%stuff not used:
% fou_data_env=fftshift(ifft(conj(hilbert(real((time_series).*exp(-1i.*2.*pi.*central_frequency.*time)'))  )));
% omegaxis = linspace(-1/ht/2,1/ht/2,L+1); omegaxis = omegaxis(1:end-1);
% new_omegaxis=omegaxis(L/2+1:end);
% finalfoudata=fou_data_env(L/2+1:end)/max(fou_data_env(L/2+1:end));
