function [params]=case2_param_dialog_nb()
%print diaglon for no bathymetry
	prompt = {'\Omega [m^{-1/2}]','kh','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
		'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
		'Shoaling ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
	dlg_title = 'Parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	
	
	try %This sets the option to the last file used.
		defaultans=importfile_buttondialog_case1('button_dialog_temp_2.txt');
		defaultans=cellstr(defaultans);
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	catch exception %in case this file doesn't exist, it uses another file.
		defaultans = {'1','1.3','50','10','11','100','0.14','0','yes','1','0.0021072','100'};
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	end
	
end