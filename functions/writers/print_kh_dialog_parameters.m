function print_kh_dialog_parameters(kh0,khfin,xvarstart,xvarstop,xvarslope)


% defaultans = {'1.01','10','120','12','100','0.06','1','1','no','1.83','0.00','100'};
% kh0 = str2double(bathymetrypar(1));
% khfin = str2double(bathymetrypar(2));
% xvarstart = str2double(bathymetrypar(3)); %[m]
% xvarstop = str2double(bathymetrypar(4)); %[m]
% xslope = str2double(bathymetrypar(5));



filename='kh_dialog_temp.txt';
fileID = fopen(filename,'w');
% fileID = fopen(button_dialog_temp_1_test,'w');

% format shortg;
datetime = clock;
fprintf(fileID,'Parameters for kh dialog\n');
fprintf(fileID,'Date\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\n\n',datetime);

fprintf(fileID,'kh_0\t%.3f\n',kh0);
fprintf(fileID,'kh_fin\t%.3f\n',khfin);
fprintf(fileID,'xvarstart\t%.3f\n',xvarstart);
fprintf(fileID,'xvarstop\t%.3f\n',xvarstop);
fprintf(fileID,'xvarslope\t%.3f\n',xvarslope);

fclose(fileID);
end
