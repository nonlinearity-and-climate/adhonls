function print_button_dialog_parameters_case2(Omega,Lx,Nper,nt,Nx,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time)
% defaultans = {'1.01','10','120','12','100','0.06','1','1','no','1.83','0.00','100'};
%  prompt = {'f_0 [1/s] .','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
%             'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
%             'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
filename='button_dialog_temp_2.txt';
fileID = fopen(filename,'w');
if 	Exportinit=='yes'
	expval=1;
else
	expval=0;
end
% fileID = fopen(button_dialog_temp_1_test,'w');

% format shortg;
datetime = clock;
fprintf(fileID,'Parameters for button dialog case 1\n');
fprintf(fileID,'Date\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\n\n',datetime);

fprintf(fileID,'Omega\t%.3f\n',Omega);
fprintf(fileID,'Lx\t%.3f\n',Lx);
fprintf(fileID,'Lt\t%.3f\n',Nper);
fprintf(fileID,'nt\t%.3f\n',nt);
fprintf(fileID,'Nx\t%.3f\n',Nx);
fprintf(fileID,'Steepness\t%.3f\n',esteep0);
fprintf(fileID,'Shoaling\t%.3f\n',shoalingflag);
fprintf(fileID,'FOD\t%.3f\n',FODflag);
% fprintf(fileID,strcat('Export\t',Exportinit,'\n'));
fprintf(fileID,'Export\t%.3f\n',expval);
fprintf(fileID,'Wm_factor\t%.3f\n',Factor);
fprintf(fileID,'Disipation\t%.3f\n',dis);
fprintf(fileID,'Output_time\t%.3f\n%',Out_time);

fclose(fileID);
end
