function [flm fullfolder] =save_filename(Omega_dim,B0,esteepness,T0_dim)
    currentFolder = pwd;
	if ~exist(strcat(currentFolder,'data'), 'dir') %check if dir already exists
	   mkdir(strcat(currentFolder,'data'))%makes the folder for preeliminary results.
    end
	folder=fullfile(currentFolder,'data');
%     folder = strcat(currentFolder,'\data\');
    format shortg;
    datetime = clock;
    date= sprintf('%4d_%.2i_%.2i-%.2i_%.2i_%.1f',datetime);
    flm=sprintf('_W_%.4f_bo_%.4f_es_%.3f_T0_%.4f',Omega_dim,B0,esteepness,T0_dim);
    flm=strcat(date,flm);
    flm = strrep(flm, '.', 'p');
    fullfolder = fullfile(folder,flm);
end
