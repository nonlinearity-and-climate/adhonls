function kh = khfun1(x,kh0,khf,xvarstart,xvarstop,xslope)
    %%
    % Gradual variation in the form of complementary error function
    % INPUT
    % x point in the propagation distance
    % kh0 initial kh
    % khf final kh
    % xvarstart/stop initial/final coordinate of kh variation
    % xslope, a normalization for the transition
    % 
    % OUTPUT
    % kh(x)
    %%
    kh = zeros(size(x));
    kh(x<=xvarstart) = kh0;
    kh(x>xvarstop) = khf;
    kh(x>xvarstart & x<=xvarstop) = khf - 0.5*(khf-kh0).*erfc((x(x>xvarstart & x<=xvarstop) - 0.5*(xvarstart+xvarstop))/xslope);
    
end