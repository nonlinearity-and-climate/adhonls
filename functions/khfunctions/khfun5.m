function h = khfun5(x,h0,hf,xvarstart,xvarstop,xslope)
    %%
    % Gradual variation in the form of linear slope
    % INPUT
    % x point in the propagation distance
    % kh0 initial kh
    % khf final kh
    % xvarstart/stop initial/final coordinate of kh variation
    % xslope, a normalization for the transition
    % 
    % OUTPUT
    % kh(x)
    %%
    h = zeros(size(x));
    h(x<=xvarstart) = h0;
    h(x>xvarstop) = hf;
    h(x>xvarstart & x<=xvarstop) = h0  + (hf-h0)./(xvarstop-xvarstart).*(x(x>xvarstart & x<=xvarstop)-xvarstart);
    
end