function f=plot_fourier_sanscarrier(u,omegaxis,Omega,X)
	f=figure('name','Fourier','position',[20 20 500 500]);
	spectrum= fftshift(ifft(u,[],1),1);

	hold on
	g0=9.81;
	max_spec=max(abs(spectrum(:,1)));
	[ d, ix ] = min( abs( abs(spectrum(:,1)) - max_spec ) );%find the critical value index
	spectrum(ix,:)=0.5*spectrum(ix-1,:)+0.5*spectrum(ix+1,:);

	UdBth = -15;
	Usp = 10*log10(abs(spectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi)));
% 	    Usp = 10*log10(abs(uspectrum.')./max(uspectrum,[],"all"));
% 	    Usp = 20*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi))./B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;

	s2=pcolor(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp );
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
    set(gca,'XDir','normal','FontSize',14);

%     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);

	for j=1:length(omegaxis)
		if Usp(end,j)>UdBth
			omega_min=j;
			break
		end
	end
	
	for j=1:length(omegaxis)
		if Usp(end,end-j)>UdBth
			omega_max=length(omegaxis)-j;
			break
		end
	end
	plotlim=[omegaxis(omega_min)*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),omegaxis(omega_max)*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)];
	xlim(plotlim);

% 	xlim([0.4*Omega*sqrt(g0)/(2*pi),2.5*Omega*sqrt(g0)/(2*pi)]);
	ylim([X(1),X(end)])
	xlabel('f [Hz]','Interpreter','latex','fontsize',16);
	ylabel('x [m]','Interpreter','latex','fontsize',16);
	ytickformat('%.1f');
	colorMap = inferno;
	colormap(colorMap);   % Apply the colormap
	cb=colorbar();
	ylabel(cb, 'dB')
	
	plot([Omega*sqrt(g0)/(2*pi),Omega*sqrt(g0)/(2*pi) ],[X(1),X(end)],'--k','linewidth',2)
	
	hold off
	
	
	
end