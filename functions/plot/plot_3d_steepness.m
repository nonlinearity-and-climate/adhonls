function f=plot_3d_steepness(u,t,X,K,tplot)
	f=figure('name','3d Steepness');
	g0=9.81;
	meshc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).').*(K.'*ones(size(t(tplot)))));
	set(gca,'YDir','normal','FontSize',14);
	xlabel('T [s]','fontsize',16);
	ylabel('X [m]','fontsize',16);
	zlabel('Steepness','fontsize',16);
	% shading interp;
	colormap hot;
end