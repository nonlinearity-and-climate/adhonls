function f = plot_parameters_1(X,Cg,Lambda,cg0,Nu)

f = figure('name','Params');
subplot(211);
plot(X,Cg,X,Lambda,'LineWidth',1.5);
% title('Pause, press any key to continue...','FontSize',20)
%legend({'h [m]','\mu [m^{-1}]'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(212);
plot(X,Nu,X,Nu./Lambda.*cg0./Cg,'LineWidth',1.5);
%legend({'\nu/ \lambda'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);

end