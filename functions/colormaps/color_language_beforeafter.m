function [s_color,e_color]=color_language_beforeafter()
	s_color=[48,117,222]./255; %color at the start of the simulation
    e_color=[223,78,192]./255; %color at the end of the simulation
end
